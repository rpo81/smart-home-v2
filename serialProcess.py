import serial
import time
import multiprocessing
 
class SerialProcess(multiprocessing.Process):
 
    def __init__(self, taskQ, resultQ):
        multiprocessing.Process.__init__(self)
        self.taskQ = taskQ
        self.resultQ = resultQ
        try:
        	self.usbPort = '/dev/ttyUSB0'
        	self.sp = serial.Serial(self.usbPort, 115200, timeout=1)
        except:
        	try:
        	 self.usbPort = '/dev/ttyUSB1'
        	 self.sp = serial.Serial(self.usbPort, 115200, timeout=1)
        	except:
		        try:
		        	self.usbPort = '/dev/ttyACM0'
		        	self.sp = serial.Serial(self.usbPort, 115200, timeout=1)
		        except:
		        	print 'did not found the serial port!: ', self.usbPort
		        	try:
		        		self.usbPort = '/dev/ttyACM1'
		        		self.sp = serial.Serial(self.usbPort, 115200, timeout=1)
		        	except:
		        		print 'did not found the serial port!: ', self.usbPort
		        		try:
			        		self.usbPort = '/dev/ttyAMA0'
			        		self.sp = serial.Serial(self.usbPort, 115200, timeout=1)
			        	except:
			        		print 'did not found the serial port!: ', self.usbPort
			        		try:
				        		self.usbPort = '/dev/ttyAMA1'
				        		self.sp = serial.Serial(self.usbPort, 115200, timeout=1)
				        	except:
				        		print 'did not found the serial port!: ', self.usbPort
 
    def close(self):
        self.sp.close()
 
    def sendData(self, data):
        print "sendData start..."
        self.sp.write(data)
        time.sleep(3)
        print "sendData done: " + data
 
    def run(self):
 
    	self.sp.flushInput()
 
        while True:
            # look for incoming tornado request
            if not self.taskQ.empty():
                task = self.taskQ.get()
 
                # send it to the arduino
                self.sp.write(task + "\n");
                print "arduino received from tornado: " + task
 
            # look for incoming serial data
            if (self.sp.inWaiting() > 0):
            	result = self.sp.readline().replace("\n", "")
 
                # send it back to tornado
            	self.resultQ.put(result)