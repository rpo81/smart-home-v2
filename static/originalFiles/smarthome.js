var fixme = function() {
  scroll(0, 0);

  var header = $(".header:visible");
  var footer = $(".footer:visible");
  var content = $(".content:visible");
  var viewport_height = $(window).height();

  var content_height = viewport_height - header.outerHeight() - footer.outerHeight();

  content_height -= (content.outerHeight() - content.height());
  content.height(content_height);
};

$(document).ready(function() {
	$(window).bind("orientationchange resize pageshow", fixme);
});


$(window).resize(function() {

});
	
$('#refreshMe1').live('click', function(event) {
	location.reload();
});

$('#refreshMe2').live('click', function(event) {
	location.reload();
});

$('#shutdown').live('click', function(event) {
	sendMessage('CMD:shutdown');
	alert('Shutdown Smart Home now');
});

$('#reboot').live('click', function(event) {
	sendMessage('CMD:reboot');
	alert('Reboot Smart Home now');
});

$('#saveHolidayMode').live('click', function(event) {
	var mode = 0;
	if ($("#holidayMode_1").is(':checked') == true)
	{
		mode = 1;
	}
	sendMessage('CMD:updateHolidayMode:' + mode);
	alert('Save holiday mode:' + mode);
});

$('#saveSwitches').live('click', function(event) {
	var json = {"switches": statusSwitches.value}

	$.ajax({  
		type: "GET",  
		url: "/",  
		data: json,
		success: function(){
			alert('New switches config saved!');
		}
	});	
});

$('#saveTimers').live('click', function(event) {
	var json = {"timers": statusTimers.value}

	$.ajax({  
		type: "GET",  
		url: "/",  
		data: json,
		success: function(){
			alert('New timers config saved!');
		}
	});	
});

/*
$('#dumpStopWatch').live('click', function(event) {
	sendMessage('CMD:dumpStopWatch:');
	alert('Dump stop watch values...');
});
*/

// Page load Home
$('#Home').live( 'pagebeforecreate',function(event){
	$.get("static/switchesHTML.txt",function(data,status){
		$("#ajaxSwitches").append(data).listview('refresh');
		$("#ajaxSwitches").trigger('create');		
	});	
});

// Page load Advanced
$('#Advanced').live( 'pagebeforecreate',function(event){
	var timerOn = {};
	var timerOff = {};
	var activeTimer = {};
	$.get("static/switchTimer.txt",function(data,status){
		var lines = data.split('\n');
		for (line in lines)
		{
			var n = lines[line].split('$');	
			var name = n[0];
			var onTime = n[1];
			var offTime = n[2];
			var active = n[3];
			timerOn[name] = onTime;
			timerOff[name] = offTime;
			activeTimer[name] = active;
			//console.log(timerOn[name]);
			//console.log(timerOff[name]);
		}
	});
	
	$.get("static/switchHolidayMode.txt",function(data,status){
		var lines = data.split('\n');
		if (lines.length > 0)
		{
			var mode = lines[0];
			if (mode == '0')
			{
				$("#holidayMode_0").attr("checked",true).checkboxradio("refresh");
				$("#holidayMode_1").attr("checked",false).checkboxradio("refresh");
				//alert('0');
			}
			else if (mode == '1')
			{
				$("#holidayMode_0").attr("checked",false).checkboxradio("refresh");
				$("#holidayMode_1").attr("checked",true).checkboxradio("refresh");
				//alert('1');
			}
		}
	});

	$.get("static/switches.txt",function(data,status){
		var lines = data.split('\n');
		var htmlStr = '';
		for (i=0; i<lines.length-1; i++)
		{
			var n = lines[i].split(';');	
			
			onTime = timerOn[n[0]];
			if (onTime == undefined)
			{
				onTime = '';
			}	
			
			offTime = timerOff[n[0]];
			if (offTime == undefined)
			{
				offTime = '';
			}		
			
			var check = 'none';
			var uncheck = 'none';
			
			var active = activeTimer[n[0]];
			if (active == undefined || active == '1')
			{
				check = 'block';
			}
			else if (active == '0')
			{
				uncheck = 'block';
			}
			
			htmlStr += '<li>\n';
			htmlStr += '	<div class="ui-grid-d">\n';
			htmlStr += '		<div class="ui-block-a">\n';
			htmlStr += '			<h4 class="ui-li-heading"></h4>\n';
			htmlStr += '			<p class="ui-li-desc">' + n[0] + '</p>\n';
			htmlStr += '		</div>\n';
			htmlStr += '		<div class="ui-block-b">\n';
			htmlStr += '			<input type="text" name="' + n[0] + '" class="timeInput" id="' + n[0] + 'TimeOn" value="' + onTime + '" data-mini="true"/>\n';
			htmlStr += '		</div>\n';
			htmlStr += '		<div class="ui-block-c">\n';
			htmlStr += '			<input type="text" name="' + n[0] + '" class="timeInput" id="' + n[0] + 'TimeOff" value="' + offTime + '" data-mini="true"/>\n';
			htmlStr += '		</div>\n';
			htmlStr += '		<div class="ui-block-d" style="margin:0px">\n';
			//htmlStr += '			<div data-role="controlgroup" data-type="horizontal" data-mini="true">\n'
			htmlStr += '				<a href="#" name="' + n[0] + '" style="margin:0px; top:3px; border-radius:8px" data-role="button" data-theme="e" data-mini="true" data-inline="true" class="updateButton" data-icon="check">Update</a>\n'
			htmlStr += '				<a href="#" name="' + n[0] + '" style="margin:0px; top:3px; border-radius:8px" data-role="button" data-theme="e" data-mini="true" data-inline="true" class="deleteButton" data-icon="delete" data-iconpos="notext">Del</a>\n'
			//htmlStr += '			</div>\n'
			htmlStr += '		</div>\n'
			htmlStr += '		<div class="ui-block-e">\n';
			htmlStr += '			<div style="position:absolute; right:15px">\n'
			htmlStr += '				<div class="myCheck" name="' + n[0] + '" id="' + n[0] + 'Checked" style="display:' + check + '" value="1">\n'
			htmlStr += '					<img src="static/Check-icon.png"/>\n'
			htmlStr += '				</div>\n'
			htmlStr += '				<div class="myCheck" name="' + n[0] + '" id="' + n[0] + 'Unchecked" style="display:' + uncheck + '" value="0">\n'
			htmlStr += '					<img src="static/Delete-icon.png"/>\n'
			htmlStr += '				</div>\n'
			htmlStr += '			</div>\n'
			htmlStr += '		</div>\n';
			htmlStr += '	</div>\n';
			htmlStr += '</li>\n';		
		}
		$("#ajaxSwitchesAdvanced").append(htmlStr).listview('refresh');
		$("#ajaxSwitchesAdvanced").trigger('create');				
	});		
});

// Page load ServerStatus
$('#ServerStatus').live( 'pagebeforeshow',function(event){
	sendMessage('CMD:getStatus');
	$.get("static/switches.txt",function(data,status){
		statusSwitches.value = data;
	});
	
	$.get("static/switchTimer.txt",function(data,status){
		statusTimers.value = data;
	});
});


// click on settings thumbnail
$("#switchThumbnail").live('click', function(event) {
	$.mobile.changePage('#myDialog', { transition: "pop", role: "dialog", reverse: true } );
});

// click on dialog thumbnail
$(".thumb").live('click', function(event) {
	var src = $(this).attr('src');
	//alert(src);		
	
	$("#switchThumbnail").html('<img id=\"switchImg\" style="width: 80px; height: 80px" src=\"' + src + '\">');
	$.mobile.changePage($('#SettingsSwitch'), 'pop', false, true); 
});

$(".mySwitchButton").live('click', function(event) {
	var val = $(this).val();
	var switchName = $(this).attr('name');
	
	if (switchName == 'AllOff')
	{
		//send special command to switch off all switches
		sendMessage('CMD:allOff');
	}
	else
	{
		var onIconId = switchName + 'OnIcon';
		var offIconId = switchName + 'OffIcon';
		
		if (val == 0)
		{
			document.getElementById(onIconId).style.display = 'none';
			document.getElementById(offIconId).style.display = 'block';
		}
		else
		{
			document.getElementById(onIconId).style.display = 'block';
			document.getElementById(offIconId).style.display = 'none';			
		}
		sendMessage('SWITCH:' + switchName + ':' + val);
	}
});

$(".myCheck").live('click', function(event) {
	event.preventDefault();
	val = $(this).attr('value')
	var switchName = $(this).attr('name');

	var checkId = switchName + 'Checked';
	var uncheckId = switchName + 'Unchecked';

	if (val == '1')
	{
		document.getElementById(checkId).style.display = 'none';
		document.getElementById(uncheckId).style.display = 'block';			
	}
	else
	{
		document.getElementById(checkId).style.display = 'block';
		document.getElementById(uncheckId).style.display = 'none';			
	}
});

$(".updateButton").live('click', function(event) {
	event.preventDefault();
	var switchName = $(this).attr('name');
	
	var str1 = switchName + 'TimeOn';
	var str2 = switchName + 'TimeOff';
	var checkId = switchName + 'Checked';
	var uncheckId = switchName + 'Unchecked';
	
	var timeOn = document.getElementById(str1).value;
	var timeOff = document.getElementById(str2).value;
	
	var timesOk = true;
	var hour = -1;
	var minutes = -1;
	
	//analyze time values
	var n = timeOn.split(';');		
	for (var i=0; i<n.length; i++)
	{
		if (n[i].length > 0)
		{
			//if (n[i].length == 5)
			//{
			
			hour = parseInt(n[i].split(':')[0], 10);
			minutes = parseInt(n[i].split(':')[1], 10);
			if(hour < 0 || hour > 23)
			{
				timesOk = false;
			}
			if(minutes < 0 || minutes > 59)
			{
				timesOk = false;
			}
			
			/*
			}
			else
			{
				timesOk = false;
			}
			*/
		}
	}
	
	n = timeOff.split(';');
	for (var i=0; i<n.length; i++)
	{
		if (n[i].length > 0)
		{
			//if (n[i].length == 5)
			//{
			
			hour = parseInt(n[i].split(':')[0], 10);
			minutes = parseInt(n[i].split(':')[1], 10);
			if(hour < 0 || hour > 23)
			{
				timesOk = false;
			}
			if(minutes < 0 || minutes > 59)
			{
				timesOk = false;
			}
			
			/*
			}
			else
			{
				timesOk = false;				
			}
			*/
		}
	}
	
	if (timesOk == true)
	{
		var checked = '0';		
		if (document.getElementById(checkId).style.display == 'block')
		{
			checked = '1';
		}	
		sendMessage('CMD:update:' + switchName + ':' + timeOn + '$' + timeOff + '$' + checked);
		alert(switchName + ' On: ' + timeOn + ' Off: ' + timeOff + ' Checked: ' + checked);
	}
	else
	{
		alert('Please input valid (HH:MM-mask) time values!');			
	}
});

$(".deleteButton").live('click', function(event) {
	event.preventDefault();
	var switchName = $(this).attr('name');
	sendMessage('CMD:delete:' + switchName);
	alert('delete: ' + switchName);
});

$('#switchForm').live('submit', function(e) {
	e.preventDefault();
	
	var name = $("#switchName").val();  
	var codeOn = $("#switchCodeOn").val();
	var codeOff = $("#switchCodeOff").val();
	var bitLength = $("#switchBitLength").val();
	var pulseWidth = $("#switchPulseWidth").val();
	var imgSrc = $("#switchImg").attr('src');
	
	var mode = 0;		
	if ($("#mode_1").is(':checked') == true)
	{
		mode = 1;
	}		
	if ($("#mode_2").is(':checked') == true)
	{
		mode = 2;
	}
	
	var protocol = 0;		
	if ($("#protocol_1").is(':checked') == true)
	{
		protocol = 1;
	}		
	if ($("#protocol_2").is(':checked') == true)
	{
		protocol = 2;
	}
	if ($("#protocol_3").is(':checked') == true)
	{
		protocol = 3;
	}
	if ($("#protocol_4").is(':checked') == true)
	{
		protocol = 4;
	}
	
	//var datastring = name + ';' + codeOn + ';' + codeOff + ';' + protocol;
	var json = {"switchName": name, 
				"switchCodeOn": codeOn,
				"switchCodeOff": codeOff, 
				"protocol": protocol,
				"bitLength": bitLength,
				"mode": mode,
				"pulseWidth": pulseWidth,
				"imgSrc": imgSrc}
	//alert(datastring);

	$.ajax({  
		type: "GET",  
		url: "/",  
		data: json,
		success: function(){
			alert('New switch was added!');
			//$("#myPopup").popup("open");
		}
	});		
	return false;
});

var socket = new WebSocket('ws://' + location.host + '/ws');	

socket.onopen = function(){  
	console.log("connected");
	sendMessage('updatedata');
}; 

socket.onmessage = function (message) {
	console.log("receiving: " + message.data);
	var n = message.data.split(':');
	if (n[0] == 'S')
	{
		switchMessage.value = n[1];			
	}
	else if (n[0] == 'SW_')
	{
		switchName = n[1];
		console.log(switchName);
		
		state = n[2];
		console.log(state);

		/*
		elem = $(document.getElementById(switchName));
		console.log(elem);			
		elem.val(state).trigger('create').slider("refresh");
		elem.trigger('slidestop');
		*/
		
		var onIconId = switchName + 'OnIcon';
		var offIconId = switchName + 'OffIcon';
		
		if (state == 0)
		{
			document.getElementById(onIconId).style.display = 'none';
			document.getElementById(offIconId).style.display = 'block';
		}
		else
		{
			document.getElementById(onIconId).style.display = 'block';
			document.getElementById(offIconId).style.display = 'none';			
		}
		
		switchMessage.value = message.data;
	}
	else if (n[0] == 'LS')
	{
		lux = n[1];
		volts = n[2];
		ldrSensorLux.value = lux + ' Lux';
		ldrSensorVolts.value = volts + ' Volts';
	}
	else if (n[0] == 'STATUS')
	{
		var temp = n[1];
		var uptime = '';
		
		for (var i=2; i<n.length-1; i++)
		{
			uptime += n[i] + ':';
		}
		uptime += n[n.length-1];
		
		$("#temp").html('Temperature: ' + temp);
		$("#uptime").html('Uptime: ' + uptime);
	}
	else
	{
		switchMessage.value = message.data;	
		//alert('popup');	
	}
};

socket.onclose = function(){
	console.log("disconnected"); 
};

sendMessage = function(message) {
	socket.send(message);
};

