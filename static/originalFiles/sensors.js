// Page load Home
$('#Sensors').live( 'pagebeforecreate',function(event){
	$.get("sensorsHTML.txt",function(data,status){
		$("#ajaxSensors").append(data).listview('refresh');
		$("#ajaxSensors").trigger('create');		
	});	
});

$('#sensorForm').live('submit', function(e) {
	e.preventDefault();
	//alert('TEst');
	var sensorName = $("#sensorName").val(); 
	var sensorCode = parseInt($("#sensorCode").val(), 10);
	var sensorType = '';
	
	if ($("#sensor_temp").is(':checked') == true)
	{
		sensorType = 'T';
	}
	if ($("#sensor_sound").is(':checked') == true)
	{
		sensorType = 'S';
	}
	if ($("#sensor_light").is(':checked') == true)
	{
		sensorType = 'L';
	}
	if ($("#sensor_motion").is(':checked') == true)
	{
		sensorType = 'M';
	}
	
	// check if sensorCode is valid
	// sensorCode must be a value between 0-15
	if ((sensorCode > -1) && (sensorCode < 16))
	{
		//alert('valid sensorCode');
		var json = { "sensorName": sensorName, "sensorCode": sensorCode, "sensorType": sensorType }	
		$.ajax({  
			type: "GET",  
			url: "/",  
			data: json,
			success: function(){
				alert('New sensor was added!');
			}
		});
	}
	else
	{
		alert('invalid sensorCode');
	}	
	return false;
});

var socket = new WebSocket('ws://' + location.host + '/ws');	

socket.onopen = function(){  
	console.log("connected");
	sendMessage('updatesensors');
}; 

socket.onmessage = function (message) {
	console.log("receiving: " + message.data);
	var n = message.data.split(':');
	if (n[0] == 'SENSOR')
	{
		sensorCode = n[1];
		//console.log(sensorCode);
		
		temp = n[2];
		//console.log(temp);
		
		sensorBattery = n[3];
		sensorTime = n[4]+':'+n[5];

		sensor = 'sensor'+sensorCode;
		elem = document.getElementById(sensor+'Value');
		if (elem != undefined)
		{
			elem.value = temp + '°C';
		}
		
		elem = document.getElementById(sensor+'Battery');
		if (elem != undefined)
		{
			elem.value = sensorBattery;
		}
		
		elem = document.getElementById(sensor+'Time');
		if (elem != undefined)
		{
			elem.value = sensorTime;
		}
	}
};

socket.onclose = function(){
	console.log("disconnected"); 
};

sendMessage = function(message) {
	socket.send(message);
};