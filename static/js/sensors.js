$('#sensorForm').live('submit', function(e) {
	e.preventDefault();
	//alert('TEst');
	var sensorName = $("#sensorName").val(); 
	var sensorCode = parseInt($("#sensorCode").val(), 10);
	var sensorType = '';
	
	if ($("#sensor_1").is(':checked') == true)
	{
		sensorType = 'DS18B20';
	}
	if ($("#sensor_2").is(':checked') == true)
	{
		sensorType = 'LM35';
	}
	if ($("#sensor_3").is(':checked') == true)
	{
		sensorType = 'LDR';
	}
	if ($("#sensor_4").is(':checked') == true)
	{
		sensorType = 'HX711';
	}
	if ($("#sensor_5").is(':checked') == true)
	{
		sensorType = 'HC_SR04';
	}
	if ($("#sensor_6").is(':checked') == true)
	{
		sensorType = 'PIR';
	}
	if ($("#sensor_7").is(':checked') == true)
	{
		sensorType = 'DHT22_Temp';
	}
	if ($("#sensor_8").is(':checked') == true)
	{
		sensorType = 'DHT22_Humi';
	}
	if ($("#sensor_9").is(':checked') == true)
	{
		sensorType = 'BMP180';
	}
	
	// check if sensorCode is valid
	// sensorCode must be a value between 0-15
	if ((sensorCode > -1) && (sensorCode < 16))
	{
		//alert('valid sensorCode');
		var json = { "sensorName": sensorName, "sensorCode": sensorCode, "sensorType": sensorType }	
		$.ajax({  
			type: "GET",  
			url: "/",  
			data: json,
			success: function(){
				alert('New sensor was added!');
			}
		});
	}
	else
	{
		alert('invalid sensorCode (0-15)');
	}	
	return false;
});