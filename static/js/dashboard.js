$(document).ready(function(){
	//alert('page loaded...');
	$.get("dashboardPanelsHTML.txt",function(data,status){
		$("#ajaxSensors").append(data + '</div>\n');	
	});
});

var socket = new WebSocket('ws://' + location.host + '/ws');	

socket.onopen = function(){  
	console.log("connected");
	sendMessage('updatesensors');
}; 

socket.onmessage = function (message) {
	console.log("receiving: " + message.data);
	var n = message.data.split(':');
	if (n[0] == 'SENSOR')
	{
		sensorId = n[1];	
		sensorType = sensorId.split('@')[0];	
		sensorValue = n[2];		
		sensorBattery = n[3];
		sensorTime = n[4]+':'+n[5];
		sensorMin = n[6];
		sensorMax = n[7];

		sensor = 'sensor'+sensorId;
		
		if (sensorType == 'Energy')
		{
			sensorTime = n[4]+':'+n[5]+':'+n[6];
			sensorMin = n[7];
			sensorMax = n[8];			
		}		
		
		elem = document.getElementById(sensor+'Panel');
		if (elem != undefined)
		{
			sensorName = elem.innerHTML.split('@');
			elem.innerHTML = sensorName[0] + ' @ ' + sensorTime;
		}
		
		// update sensor value
		elem = document.getElementById(sensor+'Value');
		if (elem != undefined)
		{
			elem.innerHTML = '<strong>' + sensorValue + '</strong>';
			if (sensorType == "HX711")
			{
				if (sensorValue > 10)
				{
					elem.style.color = "#d20000";
				}
				else
				{
					elem.style.color = "#3c9f00";
				}
			}
		}
		
		
		// update sensor battery or total energy for energy sensors
		if (sensorType != 'Energy')
		{
			elem = document.getElementById(sensor+'Battery');
			if (elem != undefined)
			{
				// 0 = 3.0V
				// 7 = 4.5V
				sensorBattery = Number((((sensorBattery / 7) * 1.5) + 3.0).toFixed(2));
				elem.innerHTML = '<small>' + sensorBattery + 'V</small>';
			}
		}
		else
		{
			elem = document.getElementById(sensor+'Battery');
			if (elem != undefined)
			{
				sensorBattery /= 1000.0;
				elem.innerHTML = '<small>' + sensorBattery + 'kWh</small>';
			}
		}
		
		// update min max with units
		if (sensorType == 'DS18B20' || sensorType == 'LM35' || sensorType == 'DHT22_Temp')
		{
			elem = document.getElementById(sensor+'Min');
			if (elem != undefined)
			{
				elem.innerHTML = '<small>Min: '+ sensorMin +'°C</small>';
			}
			
			elem = document.getElementById(sensor+'Max');
			if (elem != undefined)
			{
				elem.innerHTML = '<small>Max: '+ sensorMax +'°C</small>';
			}
		}
		
		if (sensorType == 'DHT22_Humi')
		{
			elem = document.getElementById(sensor+'Min');
			if (elem != undefined)
			{
				elem.innerHTML = '<small>Min: '+ sensorMin +'%</small>';
			}
			
			elem = document.getElementById(sensor+'Max');
			if (elem != undefined)
			{
				elem.innerHTML = '<small>Max: '+ sensorMax +'%</small>';
			}
		}
		
		if (sensorType == 'Energy')
		{
			elem = document.getElementById(sensor+'Min');
			if (elem != undefined)
			{
				elem.innerHTML = '<small>Min: '+ sensorMin +'W</small>';
			}
			
			elem = document.getElementById(sensor+'Max');
			if (elem != undefined)
			{
				elem.innerHTML = '<small>Max: '+ sensorMax +'W</small>';
			}
		}
	}
};

socket.onclose = function(){
	console.log("disconnected"); 
};

sendMessage = function(message) {
	socket.send(message);
};