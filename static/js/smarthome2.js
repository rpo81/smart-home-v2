$('#refreshMe1').bind( "click", function(event, ui) {
	location.reload();
});

$('#refreshMe2').bind( "click", function(event, ui) {
	location.reload();
});

//------------------------------------------------------
	
var text2UpdateId = undefined;

//$('.timeInput').bind( "click", function(event, ui) {
$(document).on('click', '.timeInput', function(){ 
	// function will be called every time the user clicked the time input
	//alert("onTimeInput");
	$("#myPopup").popup( "open", {"y":0});
	
	text2UpdateId = $(this).attr('id');
	
	//var textValue = $(this).attr('value');
	var elem = document.getElementById(text2UpdateId);	
	var textValue = elem.value;
	var valArr = textValue.split(';');	

	$("#myTable").find("tr:gt(0)").remove();
	
	for (v in valArr)
	{
		console.log(valArr[v]);
		myTime = valArr[v].split('-')[0];
		flag = parseInt(valArr[v].split('-')[1]);
		console.log(flag);
		
		chk1 = "";
		chk2 = "";
		chk3 = "";
		chk4 = "";
		chk5 = "";
		chk6 = "";
		chk7 = "";
		
		if (flag & 1) {	chk1 = " checked=\"true\"";}
		if (flag & 2) {	chk2 = " checked=\"true\"";}
		if (flag & 4) {	chk3 = " checked=\"true\"";}
		if (flag & 8) {	chk4 = " checked=\"true\"";}
		if (flag & 16){ chk5 = " checked=\"true\"";}
		if (flag & 32){ chk6 = " checked=\"true\"";}
		if (flag & 64){	chk7 = " checked=\"true\"";}

		row = "";
		row += "<tr>\n";
		row += "<th><input type=\"text\" id=\"time"+ v.valueOf() +"\" value=\""+ myTime +"\" data-role=\"datebox\" ";
		row += "data-options=\'{\"mode\":\"timebox\", \"useLang\":\"en\", \"useInlineBlind\":true}\'></th>\n";
		row += "<td>\n";
		row += "<fieldset data-role=\"controlgroup\" data-type=\"horizontal\">\n";
		row += "<label><input type=\"checkbox\" id=\"checkbox-mo"+ v.valueOf() +"\" data-mini=\"true\""+ chk1 +">Mo</label>\n";
		row += "<label><input type=\"checkbox\" id=\"checkbox-di"+ v.valueOf() +"\" data-mini=\"true\""+ chk2 +">Di</label>\n";
		row += "<label><input type=\"checkbox\" id=\"checkbox-mi"+ v.valueOf() +"\" data-mini=\"true\""+ chk3 +">Mi</label>\n";
		row += "<label><input type=\"checkbox\" id=\"checkbox-do"+ v.valueOf() +"\" data-mini=\"true\""+ chk4 +">Do</label>\n";
		row += "<label><input type=\"checkbox\" id=\"checkbox-fr"+ v.valueOf() +"\" data-mini=\"true\""+ chk5 +">Fr</label>\n";
		row += "<label><input type=\"checkbox\" id=\"checkbox-sa"+ v.valueOf() +"\" data-mini=\"true\""+ chk6 +">Sa</label>\n";
		row += "<label><input type=\"checkbox\" id=\"checkbox-so"+ v.valueOf() +"\" data-mini=\"true\""+ chk7 +">So</label>\n";
		row += "</fieldset>\n";
		row += "</td>\n";
		row += "</tr>\n";
		
		$('#myTable tr:last').after(row);
	}
	$( "#myTable" ).trigger('create');
});

$('#addTime').bind( "click", function(event, ui) {
	//alert("Add new time here");
	var num = $('#myTable tr').length -1;
	
	row = "";
	row += "<tr>\n";
	row += "<th><input type=\"text\" id=\"time"+ num.valueOf() +"\" data-role=\"datebox\" ";
	row += "data-options=\'{\"mode\":\"timebox\", \"useLang\":\"en\", \"useInlineBlind\":true}\'></th>\n";
	row += "<td>\n";
	row += "<fieldset data-role=\"controlgroup\" data-type=\"horizontal\">\n";
	row += "<label><input type=\"checkbox\" id=\"checkbox-mo"+ num.valueOf() +"\" data-mini=\"true\">Mo</label>\n";
	row += "<label><input type=\"checkbox\" id=\"checkbox-di"+ num.valueOf() +"\" data-mini=\"true\">Di</label>\n";
	row += "<label><input type=\"checkbox\" id=\"checkbox-mi"+ num.valueOf() +"\" data-mini=\"true\">Mi</label>\n";
	row += "<label><input type=\"checkbox\" id=\"checkbox-do"+ num.valueOf() +"\" data-mini=\"true\">Do</label>\n";
	row += "<label><input type=\"checkbox\" id=\"checkbox-fr"+ num.valueOf() +"\" data-mini=\"true\">Fr</label>\n";
	row += "<label><input type=\"checkbox\" id=\"checkbox-sa"+ num.valueOf() +"\" data-mini=\"true\">Sa</label>\n";
	row += "<label><input type=\"checkbox\" id=\"checkbox-so"+ num.valueOf() +"\" data-mini=\"true\">So</label>\n";
	row += "</fieldset>\n";
	row += "</td>\n";
	row += "</tr>\n";
	
	$('#myTable tr:last').after(row);
	$( "#myTable" ).trigger('create');
});

$('#removeTime').bind( "click", function(event, ui) {
	// remove entry	
	$('#myTable tr:last').remove();
});

$('#closeTime').bind( "click", function(event, ui) {
	// close popup
	$("#myPopup").popup( "close" );
});

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

$('#saveTime').bind( "click", function(event, ui) {
	// save time values and close popup
	//console.log(text2Update);
	if (text2UpdateId != undefined)
	{
		var len = $('#myTable tr').length - 1;
		//console.log('len:' + len);
		var result = "";
		
		
		// Loop through grabbing everything
		var date;
		
		for (var i=0; i<len; i++)
		{
			date = eval('$("#myTable tr").find("#time'+ i.valueOf() +'").datebox("getTheDate");');
			//console.log(date);
			
			//get flag for the selected days
			var flag = 0;
			var chk;
			
			//Montag
			chk = eval('$("#myTable tr").find("#checkbox-mo'+ i.valueOf() +'").is(":checked")');
			if (chk)
			{
				flag |= 1;
			}
			//Dienstag
			chk = eval('$("#myTable tr").find("#checkbox-di'+ i.valueOf() +'").is(":checked")');
			if (chk)
			{
				flag |= 2;
			}
			//Mittwoch
			chk = eval('$("#myTable tr").find("#checkbox-mi'+ i.valueOf() +'").is(":checked")');
			if (chk)
			{
				flag |= 4;
			}
			//Donnerstag
			chk = eval('$("#myTable tr").find("#checkbox-do'+ i.valueOf() +'").is(":checked")');
			if (chk)
			{
				flag |= 8;
			}
			//Freitag
			chk = eval('$("#myTable tr").find("#checkbox-fr'+ i.valueOf() +'").is(":checked")');
			if (chk)
			{
				flag |= 16;
			}
			//Samstag
			chk = eval('$("#myTable tr").find("#checkbox-sa'+ i.valueOf() +'").is(":checked")');
			if (chk)
			{
				flag |= 32;
			}
			//Sonntag
			chk = eval('$("#myTable tr").find("#checkbox-so'+ i.valueOf() +'").is(":checked")');
			if (chk)
			{
				flag |= 64;
			}
			
			if (flag>0)
			{
				if(i>0)
				{
					result += ";";
				}
				result += addZero(date.getHours()).valueOf() + ":" + addZero(date.getMinutes()).valueOf();
				result += "-" + flag.valueOf();
			}
			else
			{
				//console.log('flag:'+ flag);
			}
		}	
		//console.log('Result:' + result);
		
		var elem = document.getElementById(text2UpdateId);
		elem.value = result;		
	}
		
	$("#myPopup").popup( "close" );
});

$( "#myPopup" ).on( "popupbeforeposition", function( event, ui ) {
	//alert("popupbeforeposition");
	//var timeString = 
});

//------------------------------------------------------

$('#shutdown').bind( "click", function(event, ui) {
	sendMessage('CMD:shutdown');
	alert('Shutdown Smart Home now');
});

$('#reboot').bind( "click", function(event, ui) {
	sendMessage('CMD:reboot');
	alert('Reboot Smart Home now');
});

$('#saveHolidayMode').bind( "click", function(event, ui) {
	var mode = 0;
	if ($("#holidayMode_1").is(':checked') == true)
	{
		mode = 1;
	}
	sendMessage('CMD:updateHolidayMode:' + mode);
	alert('Save holiday mode:' + mode);
});

$('#saveSwitches').bind( "click", function(event, ui) {
	var json = {"switches": statusSwitches.value}

	$.ajax({  
		type: "GET",  
		url: "/",  
		data: json,
		success: function(){
			alert('New switches config saved!');
		}
	});	
});

$('#saveTimers').bind( "click", function(event, ui) {
	var json = {"timers": statusTimers.value}

	$.ajax({  
		type: "GET",  
		url: "/",  
		data: json,
		success: function(){
			alert('New timers config saved!');
		}
	});	
});

$('#updateThumbs').bind( "click", function(event, ui) {
	sendMessage('CMD:UpdateThumbs');
	alert('Update your thumbnails ...');
});


$('#autofill1').bind( "click", function(event, ui) {
	event.preventDefault();
    // parsing switchMessage.value
    var n = switchMessage.value.split(' ');	
    if(n.length > 10)
    {
    	switch(n[2])
    	{
	    	case '1':
	    		$("#protocol_1").attr("checked",true).checkboxradio("refresh");
	    		$("#protocol_2").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_3").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_4").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_5").attr("checked",false).checkboxradio("refresh");
	    		break;
	    		
	    	case '2':
	    		$("#protocol_1").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_2").attr("checked",true).checkboxradio("refresh");
	    		$("#protocol_3").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_4").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_5").attr("checked",false).checkboxradio("refresh");
	    		break;
	    		
	    	case '3':
	    		$("#protocol_1").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_2").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_3").attr("checked",true).checkboxradio("refresh");
	    		$("#protocol_4").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_5").attr("checked",false).checkboxradio("refresh");
	    		break;
	    		
	    	case '4':
	    		$("#protocol_1").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_2").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_3").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_4").attr("checked",true).checkboxradio("refresh");
	    		$("#protocol_5").attr("checked",false).checkboxradio("refresh");
	    		break;
	    		
	    	case '5':
	    		$("#protocol_1").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_2").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_3").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_4").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_5").attr("checked",true).checkboxradio("refresh");
	    		break;
    	}
    	
    	if ($("#mode_1").is(':checked') == true)
		{
			switchCodeOn.value = n[4];
		}		
		if ($("#mode_2").is(':checked') == true)
		{
			switchCodeOn.value = n[6];
		}
	    switchBitLength.value = n[8];
	    switchPulseWidth.value = n[11];
    }
    return false;
});

$('#autofill2').bind( "click", function(event, ui) {
	event.preventDefault();
     // parsing switchMessage.value
    var n = switchMessage.value.split(' ');	
    if(n.length > 10)
    {
    	switch(n[2])
    	{
	    	case '1':
	    		$("#protocol_1").attr("checked",true).checkboxradio("refresh");
	    		$("#protocol_2").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_3").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_4").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_5").attr("checked",false).checkboxradio("refresh");
	    		break;
	    		
	    	case '2':
	    		$("#protocol_1").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_2").attr("checked",true).checkboxradio("refresh");
	    		$("#protocol_3").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_4").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_5").attr("checked",false).checkboxradio("refresh");
	    		break;
	    		
	    	case '3':
	    		$("#protocol_1").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_2").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_3").attr("checked",true).checkboxradio("refresh");
	    		$("#protocol_4").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_5").attr("checked",false).checkboxradio("refresh");
	    		break;
	    		
	    	case '4':
	    		$("#protocol_1").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_2").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_3").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_4").attr("checked",true).checkboxradio("refresh");
	    		$("#protocol_5").attr("checked",false).checkboxradio("refresh");
	    		break;
	    		
	    	case '5':
	    		$("#protocol_1").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_2").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_3").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_4").attr("checked",false).checkboxradio("refresh");
	    		$("#protocol_5").attr("checked",true).checkboxradio("refresh");
	    		break;	    	
    	}
    	
    	if ($("#mode_1").is(':checked') == true)
		{
			switchCodeOff.value = n[4];
		}		
		if ($("#mode_2").is(':checked') == true)
		{
			switchCodeOff.value = n[6];
		}
	    switchBitLength.value = n[8];
	    switchPulseWidth.value = n[11];
    }
    return false;
});

// Page load Home
$(document).on('pagebeforecreate', '#Home', function(event){
	$.get("static/switchesHTML.txt",function(data,status){
		$("#ajaxSwitches").append(data).listview('refresh');
		$("#ajaxSwitches").trigger('create');		
	});	
});

// Page load SettingsSwitch
$(document).on('pagebeforecreate', '#SettingsSwitch', function(event){
	$.get("static/thumbnails.txt",function(data,status){
		$("#thumbnailsInsert").append(data);
		$("#thumbnailsInsert").trigger('create');		
	});
});

// Page load Advanced
$(document).on('pagebeforecreate', '#Advanced', function(event){
	var timerOn = {};
	var timerOff = {};
	var activeTimer = {};
	$.get("static/switchTimer.txt",function(data,status){
		var lines = data.split('\n');
		for (line in lines)
		{
			var n = lines[line].split('$');	
			var name = n[0];
			var onTime = n[1];
			var offTime = n[2];
			var active = n[3];
			timerOn[name] = onTime;
			timerOff[name] = offTime;
			activeTimer[name] = active;
			//console.log(timerOn[name]);
			//console.log(timerOff[name]);
		}
	});
	
	$.get("static/switchHolidayMode.txt",function(data,status){
		var lines = data.split('\n');
		if (lines.length > 0)
		{
			var mode = lines[0];
			if (mode == '0')
			{
				$("#holidayMode_0").attr("checked",true).checkboxradio("refresh");
				$("#holidayMode_1").attr("checked",false).checkboxradio("refresh");
				//alert('0');
			}
			else if (mode == '1')
			{
				$("#holidayMode_0").attr("checked",false).checkboxradio("refresh");
				$("#holidayMode_1").attr("checked",true).checkboxradio("refresh");
				//alert('1');
			}
		}
	});

	$.get("static/switches.txt",function(data,status){
		var lines = data.split('\n');
		var htmlStr = '';
		for (i=0; i<lines.length-1; i++)
		{
			var n = lines[i].split(';');	
			
			onTime = timerOn[n[0]];
			if (onTime == undefined)
			{
				onTime = '';
			}	
			
			offTime = timerOff[n[0]];
			if (offTime == undefined)
			{
				offTime = '';
			}		
			
			var check = 'none';
			var uncheck = 'none';
			
			var active = activeTimer[n[0]];
			if (active == undefined || active == '1')
			{
				check = 'block';
			}
			else if (active == '0')
			{
				uncheck = 'block';
			}
			
			htmlStr += '<li>\n';
			htmlStr += '	<div class="ui-grid-d">\n';
			htmlStr += '		<div class="ui-block-a">\n';
			htmlStr += '			<h4 class="ui-li-heading"></h4>\n';
			htmlStr += '			<p class="ui-li-desc">' + n[0] + '</p>\n';
			htmlStr += '		</div>\n';
			htmlStr += '		<div class="ui-block-b">\n';
			htmlStr += '			<input type="text" name="' + n[0] + '" class="timeInput" id="' + n[0] + 'TimeOn" value="' + onTime + '" data-mini="true"/>\n';
			htmlStr += '		</div>\n';
			htmlStr += '		<div class="ui-block-c">\n';
			htmlStr += '			<input type="text" name="' + n[0] + '" class="timeInput" id="' + n[0] + 'TimeOff" value="' + offTime + '" data-mini="true"/>\n';
			htmlStr += '		</div>\n';
			htmlStr += '		<div class="ui-block-d" style="margin:0px">\n';
			//htmlStr += '			<div data-role="controlgroup" data-type="horizontal" data-mini="true">\n'
			htmlStr += '				<a href="#" name="' + n[0] + '" style="margin:0px; top:3px; border-radius:8px" data-role="button" data-mini="true" data-inline="true" class="updateButton" data-icon="check">Update</a>\n'
			htmlStr += '				<a href="#" name="' + n[0] + '" style="margin:0px; top:3px; border-radius:8px" data-role="button" data-mini="true" data-inline="true" class="deleteButton" data-icon="delete" data-iconpos="notext">Del</a>\n'
			//htmlStr += '			</div>\n'
			htmlStr += '		</div>\n'
			htmlStr += '		<div class="ui-block-e">\n';
			htmlStr += '			<div style="position:absolute; right:15px">\n'
			htmlStr += '				<div class="myCheck" name="' + n[0] + '" id="' + n[0] + 'Checked" style="display:' + check + '" value="1">\n'
			htmlStr += '					<img src="static/img/Check-icon.png"/>\n'
			htmlStr += '				</div>\n'
			htmlStr += '				<div class="myCheck" name="' + n[0] + '" id="' + n[0] + 'Unchecked" style="display:' + uncheck + '" value="0">\n'
			htmlStr += '					<img src="static/img/Delete-icon.png"/>\n'
			htmlStr += '				</div>\n'
			htmlStr += '			</div>\n'
			htmlStr += '		</div>\n';
			htmlStr += '	</div>\n';
			htmlStr += '</li>\n';		
		}
		$("#ajaxSwitchesAdvanced").append(htmlStr).listview('refresh');
		$("#ajaxSwitchesAdvanced").trigger('create');				
	});		
});

// Page load ServerStatus
$(document).on('pagebeforeshow', '#ServerStatus', function(){           
	$.get("static/switches.txt",function(data,status){
		statusSwitches.value = data;
	});
	
	$.get("static/switchTimer.txt",function(data,status){
		statusTimers.value = data;
	});
	sendMessage('CMD:getStatus');
});


// click on settings thumbnail
$(document).on('click', '#switchThumbnail', function(){ 
	$.mobile.changePage('#myDialog', 'pop', false, true);
});


// click on dialog thumbnail
$(document).on('click', '.myThumb', function(){ 
	var id = $(this).attr('id');
	//alert(id);	
	
	if (id != 'switchImg')
	{
		// <div id="switchImg" class="myThumb" style="background-image: url(static/img/png/steckdose.png)"></div>
		$("#switchThumbnail").html('<div id=\"switchImg\" class=\"myThumb\" style=\"background-image: url(static/img/thumbs/' + id + '.jpg)\" src=\"' + id + '\"></div>');
		$.mobile.changePage('#SettingsSwitch', 'pop', false, true);		
	}
});


$(document).on('click', '.mySwitchButton', function(){ 
	var val = $(this).attr('value');
	var switchName = $(this).attr('name');
	
	if (switchName == 'AllOff')
	{
		//send special command to switch off all switches
		sendMessage('CMD:allOff');
	}
	else
	{
		var onIconId = switchName + 'OnIcon';
		var offIconId = switchName + 'OffIcon';
		var timeId = switchName + 'Time';
		
		d = new Date();
		switchTime = addZero(d.getHours()).valueOf() + ":" + addZero(d.getMinutes()).valueOf();
		
		if (val == 0)
		{
			document.getElementById(onIconId).style.display = 'none';
			document.getElementById(offIconId).style.display = 'block';
		}
		else
		{
			document.getElementById(onIconId).style.display = 'block';
			document.getElementById(offIconId).style.display = 'none';			
		}
		elemTime = document.getElementById(timeId);
		elemTime.style.display = 'block';
		elemTime.innerHTML = switchTime;
		
		sendMessage('SWITCH:' + switchName + ':' + val);
	}
});

$(document).on('click', '.myCheck', function(){ 
	event.preventDefault();
	val = $(this).attr('value')
	var switchName = $(this).attr('name');

	var checkId = switchName + 'Checked';
	var uncheckId = switchName + 'Unchecked';

	if (val == '1')
	{
		document.getElementById(checkId).style.display = 'none';
		document.getElementById(uncheckId).style.display = 'block';			
	}
	else
	{
		document.getElementById(checkId).style.display = 'block';
		document.getElementById(uncheckId).style.display = 'none';			
	}
});

$(document).on('click', '.updateButton', function(event){ 
	event.preventDefault();
	var switchName = $(this).attr('name');
	
	var str1 = switchName + 'TimeOn';
	var str2 = switchName + 'TimeOff';
	var checkId = switchName + 'Checked';
	var uncheckId = switchName + 'Unchecked';
	
	var timeOn = document.getElementById(str1).value;
	var timeOff = document.getElementById(str2).value;
	
	var timesOk = true;
	var hour = -1;
	var minutes = -1;
	
	//analyze time values
	var n = timeOn.split(';');		
	for (var i=0; i<n.length; i++)
	{
		if (n[i].length > 0)
		{
			//if (n[i].length == 5)
			//{
			
			hour = parseInt(n[i].split(':')[0], 10);
			minutes = parseInt(n[i].split(':')[1], 10);
			if(hour < 0 || hour > 23)
			{
				timesOk = false;
			}
			if(minutes < 0 || minutes > 59)
			{
				timesOk = false;
			}
			
			/*
			}
			else
			{
				timesOk = false;
			}
			*/
		}
	}
	
	n = timeOff.split(';');
	for (var i=0; i<n.length; i++)
	{
		if (n[i].length > 0)
		{
			//if (n[i].length == 5)
			//{
			
			hour = parseInt(n[i].split(':')[0], 10);
			minutes = parseInt(n[i].split(':')[1], 10);
			if(hour < 0 || hour > 23)
			{
				timesOk = false;
			}
			if(minutes < 0 || minutes > 59)
			{
				timesOk = false;
			}
			
			/*
			}
			else
			{
				timesOk = false;				
			}
			*/
		}
	}
	
	if (timesOk == true)
	{
		var checked = '0';		
		if (document.getElementById(checkId).style.display == 'block')
		{
			checked = '1';
		}	
		sendMessage('CMD:update:' + switchName + ':' + timeOn + '$' + timeOff + '$' + checked);
		alert(switchName + ' On: ' + timeOn + ' Off: ' + timeOff + ' Checked: ' + checked);
	}
	else
	{
		alert('Please input valid (HH:MM-mask) time values!');			
	}
});

$(document).on('click', '.deleteButton', function(){ 
	event.preventDefault();
	var switchName = $(this).attr('name');
	sendMessage('CMD:delete:' + switchName);
	alert('delete: ' + switchName);
});

$(document).on('submit', '#switchForm', function(){ 
	event.preventDefault();
	
	var name = $("#switchName").val();  
	var codeOn = $("#switchCodeOn").val();
	var codeOff = $("#switchCodeOff").val();
	var bitLength = $("#switchBitLength").val();
	var pulseWidth = $("#switchPulseWidth").val();
	//var imgSrc = $("#switchImg").attr('class');
	var imgSrc = $("#switchImg").attr('src');
	
	var mode = 0;		
	if ($("#mode_1").is(':checked') == true)
	{
		mode = 1;
	}		
	if ($("#mode_2").is(':checked') == true)
	{
		mode = 2;
	}
	
	var protocol = 0;		
	if ($("#protocol_1").is(':checked') == true)
	{
		protocol = 1;
	}		
	else if ($("#protocol_2").is(':checked') == true)
	{
		protocol = 2;
	}
	else if ($("#protocol_3").is(':checked') == true)
	{
		protocol = 3;
	}
	else if ($("#protocol_4").is(':checked') == true)
	{
		protocol = 4;
	}
	else if ($("#protocol_5").is(':checked') == true)
	{
		protocol = 5;
	}
	
	//var datastring = name + ';' + codeOn + ';' + codeOff + ';' + protocol;
	var json = {"switchName": name, 
				"switchCodeOn": codeOn,
				"switchCodeOff": codeOff, 
				"protocol": protocol,
				"bitLength": bitLength,
				"mode": mode,
				"pulseWidth": pulseWidth,
				"imgSrc": imgSrc}
	//alert(datastring);

	$.ajax({  
		type: "GET",  
		url: "/",  
		data: json,
		success: function(){
			alert('New switch was added!');
			//$("#myPopup").popup("open");
		}
	});		
	return false;
});

var socket = new WebSocket('ws://' + location.host + '/ws');	

socket.onopen = function(){  
	console.log("connected");
	sendMessage('updatedata');
}; 

socket.onmessage = function (message) {
	//console.log("receiving: " + message.data);
	var n = message.data.split(':');
	if (n[0] == 'S')
	{
		switchMessage.value = n[1];			
	}
	else if (n[0] == 'SW_')
	{
		switchName = n[1];
		console.log(switchName);
		
		state = n[2];
		console.log(state);
		
		switchTime = n[3] + ":" + n[4]; 

		/*
		elem = $(document.getElementById(switchName));
		console.log(elem);			
		elem.val(state).trigger('create').slider("refresh");
		elem.trigger('slidestop');
		*/
		
		var onIconId = switchName + 'OnIcon';
		var offIconId = switchName + 'OffIcon';
		var timeId = switchName + 'Time';
		
		if (state == 0)
		{
			var elemOn = document.getElementById(onIconId);
			if (elemOn != null)
			{
				elemOn.style.display = 'none';
			}
			
			var elemOff = document.getElementById(offIconId)
			if(elemOff != null)
			{
				elemOff.style.display = 'block';
			}
		}
		else
		{
			var elemOn = document.getElementById(onIconId);
			if (elemOn != null)
			{
				elemOn.style.display = 'block';
			}
			
			var elemOff = document.getElementById(offIconId)
			if(elemOff != null)
			{
				elemOff.style.display = 'none';
			}		
		}
		elemTime = document.getElementById(timeId);
		elemTime.style.display = 'block';
		elemTime.innerHTML = switchTime;
		
		
		//eval('$("' + timeId + '").html("' + switchTime + '")');
		//eval('$("' + timeId + '").trigger("refresh")');
		
		switchMessage.value = message.data;
	}
	else if (n[0] == 'LS')
	{
		lux = n[1];
		volts = n[2];
		ldrSensorLux.value = lux + ' Lux';
		ldrSensorVolts.value = volts + ' Volts';
	}
	else if (n[0] == 'STATUS')
	{
		var temp = n[1];
		var uptime = '';
		
		for (var i=2; i<n.length-1; i++)
		{
			uptime += n[i] + ':';
		}
		uptime += n[n.length-1];
		
		$("#temp").html('Temperature: ' + temp);
		$("#uptime").html('Uptime: ' + uptime);
	}
	else
	{
		switchMessage.value = message.data;	
		//alert('popup');	
	}
};

socket.onclose = function(){
	console.log("disconnected"); 
};

sendMessage = function(message) {
	socket.send(message);
};

