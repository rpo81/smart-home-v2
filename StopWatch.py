class StopWatch():
	def __init__(self, powerUsageKW = 0):
		self._minutes = 0
		self._hours = 0
		self._days = 0
		self._minutesDaily = 0
		self._hoursDaily = 0
		self._powerUsageKW = float(powerUsageKW)

	def setPowerUsage(self, powerUsageKW):
		self._powerUsageKW = powerUsageKW

	def add(self):
		self._minutes += 1
		if self._minutes % 60 == 0:
			self._minutes = 0
			self._hours += 1
			if self._hours % 24 == 0:
				self._hours = 0
				self._days += 1
		
		# set daily times		
		self._minutesDaily += 1		
		if self._minutesDaily % 60 == 0:
			self._minutesDaily = 0
			self._hoursDaily += 1
			if self._hoursDaily % 24 == 0:
				self._hoursDaily = 0

	def getTime(self):
		result = '%d days %d hours %d minutes' %(self._days, self._hours, self._minutes)
		return result
	
	def getTimeDaily(self):
		result = '%d hours %d minutes' %(self._hoursDaily, self._minutesDaily)
		return result

	def getHours(self):
		hours = self._days * 24
		hours += self._hours
		hours += float(self._minutes) / 60.0
		return hours
	
	def getHoursDaily(self):
		hours = self._hoursDaily
		hours += float(self._minutesDaily) / 60.0
		return hours

	def getPowerUsageKWh(self):
		usage = self._powerUsageKW * self.getHours()
		result = '%.5f KWh' % usage
		return result
	
	def getPowerUsageKWhDaily(self):
		usage = self._powerUsageKW * self.getHoursDaily()()()
		result = '%.5f KWh' % usage
		return result
	
	def resetDaily(self):
		self._minutesDaily = 0
		self._hoursDaily = 0