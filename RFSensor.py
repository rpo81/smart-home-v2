class RFSensor():
	def __init__(self):
		self.sensorType = None
		self.sensorCode = None
		self.sensorBattery = None
		self.sensorValue = None
		
		id = (125 << 24) | (7 << 17)
		self.sensorTypeDict = {
							'DS18B20'	:id | (0 << 20),
							'LM35'		:id | (1 << 20),
							'LDR'		:id | (2 << 20),
							'HX711'		:id | (3 << 20),
							'HC_SR04'	:id | (4 << 20),
							'PIR'		:id | (5 << 20),
							'DHT22_Temp':id | (6 << 20),
							'DHT22_Humi':id | (7 << 20),
							'Energy'	:id | (8 << 20),
							'BMP180'	:id | (9 << 20)}
	
	def decode(self, msg):
		msg = int(msg)
		self.sensorType = None
		self.sensorCode = None
		self.sensorBattery = None
		self.sensorValue = None
			
		# check what type of sensor we have
		for sensorType in self.sensorTypeDict:
			id = self.sensorTypeDict[sensorType]

			if ((msg >> 17) << 17) == id:
				# get sensorCode from received code
				self.sensorCode = (msg >> 10) & 0xF
					
				# get battery value from received code
				self.sensorBattery = (msg >> 14) & 7
				
				# get value from sensor
				self.sensorValue = float(msg ^ ((msg >> 10) << 10))
								
				if sensorType == 'DS18B20':					
					self.sensorValue /= 10.0
					self.sensorValue -= 55 # fix offset because of possible negative temperatures
				
				elif sensorType == 'LM35':		
					self.sensorValue /= 10.0
				
				elif sensorType == 'HX711':
					self.sensorValue /= 10.0
					
				elif sensorType == 'DHT22_Temp':					
					self.sensorValue /= 10.0
					self.sensorValue -= 40 # fix offset because of possible negative temperatures
					
				elif sensorType == 'DHT22_Humi':					
					self.sensorValue /= 10.0
					
				elif sensorType == 'Energy':
					self.sensorValue = float(msg ^ ((msg >> 16) << 16))
					self.sensorCode = 0
				
				elif sensorType == 'BMP180':
					self.sensorValue = float(msg ^ ((msg >> 16) << 16))
					self.sensorValue /= 10.0
					self.sensorCode = 0
					
				self.sensorType = sensorType
				return True		
		return False
	
	def dump(self):		
		print '--------- RFSensor - Dump ---------'
		print 'SensorType:', self.sensorType
		print 'SensorCode:', self.sensorCode
		print 'SensorValue:', self.sensorValue
		print 'SensorBattery:', self.sensorBattery
		
		
if __name__ == '__main__':
	import math, time
	
	
	print 'Init RFSensor'
	identifier = 125 << 24 | (6 << 20) | (7 << 17)
 
 	temp = 24.1
 	print temp
 	
 	#temp += 55.0
 	temp += 40.0
 	print temp
 	
	valueA = int(temp)
	valueB = int(math.ceil((temp - valueA) * 10.0))
	
	#set sensorBattery and value
	code2Send = (identifier | (7 << 14)) + ((valueA*10)+valueB)
	
	#set sensorCode
	code2Send |= (1 << 10)
	
	print 'code2Send=', code2Send
	print ''
	print time.strftime('%H:%M:%S')
	print time.time()
	print time.ctime() 
	s = 1452421195.8
	print s
	print time.strftime('%H:%M:%S', time.localtime(s))
	
	print time.localtime(s)
	print 'Diff: ', time.time() - s
	print ''

	rfs = RFSensor()
	if rfs.decode('2107516956'):
		rfs.dump()
	else:
		print 'no valid sensor'