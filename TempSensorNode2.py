import time, math

class TempSensorNodes():
	def __init__(self, size=720, filerSize = 4):
		self.sensorDict = {}
		self.sensorDictAvg = {}
		self.sensorDictCounter = {}
		self._size = size
		self._avgFilterSize = filerSize
		
	def getSensorList(self):
		return self.sensorDict.keys()
	
	def shiftValue(self, sensorCode):
		myList = self.sensorDict.get(sensorCode)
		if myList != None:
			myList.insert(0, None)
			myList.pop()
			
			# check if we have # new values and average them
			sCnt = self.sensorDictCounter.get(sensorCode)
			if sCnt != None:
				self.sensorDictCounter[sensorCode] += 1
			else:
				self.sensorDictCounter[sensorCode] = 1
				
			if self.sensorDictCounter[sensorCode] == self._avgFilterSize:
				self.sensorDictCounter[sensorCode] = 0
				avg = 0
				cnt = 0
				for i in range(self._avgFilterSize):
					if myList[i] != None:
						avg += myList[i]
						cnt += 1
				if cnt > 0:
					avg /= float(cnt)
				else:
					avg = None
				
				avgList = self.sensorDictAvg.get(sensorCode)
				if avgList != None:
					avgList.insert(0, avg)
					avgList.pop()
				else:
					avgList = [None]*(self._size/self._avgFilterSize)
					avgList[0] = avg
					self.sensorDictAvg[sensorCode] = avgList

		else:
			myList = [None]*self._size
			self.sensorDict[sensorCode] = myList
			self.sensorDictCounter[sensorCode] = 1
			
	def replaceValue(self, sensorCode, value):
		myList = self.sensorDict.get(sensorCode)
		if myList != None:
			myList[0] = value
			#self.sensorDictCounter[sensorCode] += 1
		else:
			myList = [None]*self._size
			myList[0] = value
			self.sensorDict[sensorCode] = myList
			#self.sensorDictCounter[sensorCode] = 1
			
	def getValue(self, sensorCode, index):
		myList = self.sensorDict.get(sensorCode)
		if myList != None:
			if index < self._size:
				return myList[index]
			else:
				return None
		else:
			return None
			
	def getMinimum(self, sensorCode):		
		myList = self.sensorDict.get(sensorCode)
		if myList != None:
			tmp = []
			for i in myList:
				if i != None:
					tmp.append(i)
			if len(tmp) > 0:
				return min(tmp)
		return -0.0
	
	def getMaximum(self, sensorCode):
		myList = self.sensorDict.get(sensorCode)
		if myList != None:
			if len(myList) > 0:
				myMax = max(myList)
				if myMax != None:
					return myMax
		return 0.0
	
	def dump(self, sensorCode):
		myList = self.sensorDict.get(sensorCode)
		if myList != None:
			rev = list(myList)
			rev.reverse()
			return rev
		
	def dumpAvg(self, sensorCode):
		avgList = self.sensorDictAvg.get(sensorCode)
		#print avgList
		
		if avgList != None:
			rev = list(avgList)
			rev.reverse()
			return rev

if __name__ == '__main__':	
	import random
	tempSensorNodes = TempSensorNodes(1440, 60)
	for i in range(600):
		tempSensorNodes.shiftValue(1)
		tempSensorNodes.replaceValue(1, (23 + (random.random()*10)))
		tempSensorNodes.shiftValue(2)
		tempSensorNodes.replaceValue(2, (23 + (random.random()*2)))
	
	print len(tempSensorNodes.dump(1)), len(tempSensorNodes.dumpAvg(1))
	
	print tempSensorNodes.getValue(1, 0)
	print tempSensorNodes.getValue(1, 599)
	