#!/usr/bin/python27
 
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.gen
from tornado.options import define, options
import random, string 
import Image
 
import time
import multiprocessing
import serialProcess
import os
import pygal
from pygal.style import Style
from TempSensorNode2 import TempSensorNodes
from StopWatch import StopWatch
from RFSensor import RFSensor
import socket
import math
from thread import start_new_thread
from pynma import PyNMA
import socket
from autoremote import autoremote
import paho.mqtt.client as mqtt

INFLUX_DB = "192.168.178.58"
UDP_PORT = 8089
INFLUX_DB_sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP

MQTT = "192.168.178.49"
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

client = mqtt.Client()
client.on_connect = on_connect
client.connect(MQTT, 1883, 60)
client.loop_start()

 
define("port", default=8000, help="run on the given port", type=int)
 
clients = []
STATIC_PATH = '/home/pi/smart-home-v2/static/'
taskQ = None

currentSwitchStateDict = {}
switchOnTimer = {}
switchOffTimer = {}
activeTimer = {}
holidayMode = False
codes = []
lastTime =''
lastTimeTemp = ''
lastCode = None
lastSensorId = None
sensorAddedCnt = 0

switchStopWatchDict = {}
sensorDict = {}
sensorNameDict = {}

# Mask
# Mo = 1
# Di = 2
# Mi = 4
# Do = 8
# Fr = 16
# Sa = 32
# So = 64

luxAvg = 0.0
voltsAvg = 0.0
lsCnt = 0
template = None

templateSensorT = None # Template for temperature sensor
templateSensorL = None # Template for light sensor
templateSensorM = None # Template for motion sensor
templateSensorW = None # Template for weight sensor
templateSensorH = None # Template for humidity sensor

tempSensorNodes = TempSensorNodes(1440,60)
tempSensorNodesMax = TempSensorNodes(365,30)
rfs = RFSensor()

p = None
ar = None

custom_style = Style(
	background='transparent',
	plot_background='transparent',
	foreground='rgba(0, 0, 0, 0.9)',
	foreground_light='rgba(0, 0, 0, 0.9)',
	foreground_dark='rgba(0, 0, 0, 0.2)',
	opacity='.5',
	opacity_hover='.9',
	transition='300ms ease-in',
	colors=('#00b2f0', '#43d9be', '#0662ab', '#00668a', '#98eadb', '#97d959', '#033861', '#ffd541', '#7dcf30', '#3ecdff', '#daaa00'))

custom_style2 = Style(
	  background='transparent',
	  plot_background='transparent',
	  foreground='rgba(0, 0, 0, 1.0)',
	  foreground_light='rgba(0, 0, 0, 1.0)',
	  foreground_dark='rgba(0, 0, 0, 0.2)',
	  opacity='.35',
	  opacity_hover='.9',
	  transition='300ms ease-in',
	  colors=('#F67474', '#4D739A', '#4D9A4D', '#4D9A90', '#F674B5', '#FCBFAC', '#DCA199', '#B5F674', '#74F6B5', '#74B5F6', '#F6F674', '#FAAF85'))
	  #colors=('rgb(12,55,149)', 'rgb(117,38,65)', 'rgb(228,127,0)', 'rgb(159,170,0)', 'rgb(149,12,12)'))
	  
def saveSensorValue(sensorId, value):
	print 'Send Value to InfluxDB'
	sensorName = sensorNameDict[sensorId]
	INFLUX_DB_sock.sendto("%s value=%.3f" %(sensorName, value), (INFLUX_DB, UDP_PORT))
	
def autoremotePush(ar, type, name, val):
	'''
		type = "sensor", "switch"
	'''	
	print 'Autoremote Push message...'
	
	if ar != None:
		if type == "sensor":
			_msg = 'sensor=:=' + str(name) + '=:=' + str(val) 
			print _msg, ' send to autoremote'
			ar.send(_msg)
			
			client.publish("sensor/" + str(name), "%.1f" %val)
		
		elif type == "switch":
			_msg = 'switch=:=' + str(name) + '=:=' + str(val)
			print _msg, ' send to autoremote'
			ar.send(_msg)
			
			client.publish("switch/" + str(name), "%d" %val)
	else:
		print 'ar == None'
		
def autoremotePushMessage(ar, msg):
	ar.send(msg)
	
class MyStaticFileHandler(tornado.web.StaticFileHandler):
	def set_extra_headers(self, path):
	# Disable cache
		self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0')
	
class SwitchAPIHandler(tornado.web.RequestHandler):	
	def get(self,switchName):
		global currentSwitchStateDict

		print 'val:', switchName
		switchName = switchName.replace('+', ' ')
		
		result = currentSwitchStateDict.get(switchName)	
		print result
		
		if result != None:
			if result[0]:
				self.write('0')
			else:
				self.write('1')
		else:
			self.write('1')		
			
#  		keys = self.request.arguments.keys()
#  		if 'name' in keys:
#  			switchName = str(self.request.arguments['name'][-1])
#  			print switchName
#  			result = currentSwitchStateDict.get(switchName)			
#  			print 'get switch result:', result[0]
#  			self.write(result[0])
		
class SwitchAPIHandler2(tornado.web.RequestHandler):
	def post(self):
		print self.request.body

class UploadHandler(tornado.web.RequestHandler):
    def post(self):
        file1 = self.request.files['file1'][0]
        original_fname = file1['filename']
        #output_file = open("upload/" + original_fname, 'w')
        
        fileName = STATIC_PATH + 'img/thumbs/' + original_fname
        output_file = open(fileName, 'w')
        output_file.write(file1['body'])        
        output_file.close()
        
        #self.finish(original_fname + " is uploaded")
        self.render(STATIC_PATH + 'imageUpload.html')
        
        image = Image.open(fileName)
        width, height = image.size
        print width, height
        
        newHeight = (width*2/3)
        if width < height: # Hochformat        	
        	offset = (height-newHeight)/2
        	croppedImg = image.crop((0,offset,width,newHeight))
        	       	
        elif newHeight > height: # Querformat aber zu flach
        	newWidth = (height*3/2)
        	offset = (width-newWidth)/2
        	croppedImg = image.crop((offset,0,newWidth,height))
        	
        else: # Querformat
        	offset = (height-newHeight)/2
        	croppedImg = image.crop((0,offset,width,newHeight))
        
        fileName = fileName.split('.')[0] + '.jpg'
        image = croppedImg.resize((225, 150), Image.ANTIALIAS)
        image.save(fileName, quality=100)
		
class IndexHandler(tornado.web.RequestHandler):
	def get(self):
		global codes, switchOnTimer, switchOffTimer, activeTimer, currentSwitchStateDict, switchStopWatchDict, sensorNameDict
		self.render('index.html')
		
		print 'GET empfangen...'
		print 'you wrote:', self.request.arguments
		
		keys = self.request.arguments.keys()
		
		if 'switchCodeOn' in keys and 'switchCodeOff' in keys and 'protocol' in keys and 'switchName' in keys:
			
			switchName = str(self.request.arguments['switchName'][-1])
			switchCodeOn = str(self.request.arguments['switchCodeOn'][-1])
			switchCodeOff = str(self.request.arguments['switchCodeOff'][-1])
			protocol = int(self.request.arguments['protocol'][-1])
			bitLength = int(self.request.arguments['bitLength'][-1])
			mode = int(self.request.arguments['mode'][-1])
			pulseWidth = int(self.request.arguments['pulseWidth'][-1])
			imgSrc = str(self.request.arguments['imgSrc'][-1]) 
			self.request.arguments.clear()
			
			codes.append([switchName, switchCodeOn, switchCodeOff, protocol, bitLength, mode, pulseWidth, imgSrc, 1])
			currentSwitchStateDict[switchName] = [0,time.strftime('%H:%M')]
			switchStopWatchDict[switchName] = StopWatch()
			
			# Delete HTML File for switches and build it up new
			print "Delete an existing HTML file for switches..."
			file = open(STATIC_PATH + 'switchesHTML.txt', 'w')
			file.close()
			
			# sort the switch list
			codes.sort()
			
			print "Save switches information..."
			saveSwitches()
			
			print "Generating new switchHTML with sorted switches..."
			for i, code in enumerate(codes):
				blockId = i%5
				print 'BlockId:',blockId,'Switch:',code[0],'On:',code[1],'Off:',code[2],'Protocol:',str(code[3]),'Bits:',str(code[4]),'Mode:',str(code[5]),'PulseWidth:',str(code[6])
				addToSwitchHTMLFile(blockId,code[0],code[1],code[2],code[3],code[6],code[7],code[8])

		elif 'sensorName' in keys and 'sensorCode' in keys and 'sensorType' in keys:
			print 'add a new sensor'
			file = open(STATIC_PATH + 'sensors.txt', 'a')
			
			sensorName = str(self.request.arguments['sensorName'][-1])
			sensorCode = int(self.request.arguments['sensorCode'][-1])
			sensorType = str(self.request.arguments['sensorType'][-1])
			self.request.arguments.clear()
			
			out = sensorName + ';' + str(sensorCode) + ';' + sensorType + '\n'
			file.write(out)
			file.flush()
			file.close()
			
			id = str(sensorType) + '@' + str(sensorCode)
			if id not in sensorNameDict.keys():
				sensorNameDict[id] = sensorName			
				addToSensorHTMLFile(sensorName,sensorCode,sensorType)
			
		elif 'switches' in keys:
			data = str(self.request.arguments['switches'][-1])
			print 'get switches:\n', data
			
			#file = open(STATIC_PATH + 'switches.txt', 'w')
			#file.write(data)
			#file.flush()
			#file.close()
			
			# Delete HTML File for switches and build it up new
			print "Delete an existing HTML file for switches..."
			file = open(STATIC_PATH + 'switchesHTML.txt', 'w')
			file.close()
			
			# Build new HTML file for the switches
			print "Build new switch HTML file..."
		 	#file = open(STATIC_PATH + 'switches.txt', 'r')
		 	#lines = file.readlines()
		 	lines = data.split('\n')
		 	#file.close()
		 	codes = []
		 	currentSwitchStateDict.clear()
		 	
			for line in lines:
				print '->',  line
				val = line.split(';')
				if len(val) == 9:
					switchName = val[0]
					codeOn = val[1]
					codeOff = val[2]
					protocol = int(val[3])
					bitLength = int(val[4])
					mode = int(val[5])
					pulseWidth = int(val[6])
					imgSrc = val[7]
					visible = int(val[8])
					
					codes.append([switchName, codeOn, codeOff, protocol, bitLength, mode, pulseWidth, imgSrc, visible])
					currentSwitchStateDict[switchName] = [0,time.strftime('%H:%M')]
				else:
					print 'Wrong switch definition! Please check that!!!'
			
			codes.sort()
			saveSwitches()
			for i, code in enumerate(codes):
				blockId = i%5
				print 'BlockId:',blockId,'Switch:',code[0],'On:',code[1],'Off:',code[2],'Protocol:',str(code[3]),'Bits:',str(code[4]),'Mode:',str(code[5]),'PulseWidth:',str(code[6])
				addToSwitchHTMLFile(blockId,code[0],code[1],code[2],code[3],code[6],code[7],code[8])
			
		elif 'timers' in keys:
			data = str(self.request.arguments['timers'][-1])
			print 'get timers:\n', data
						
			file = open(STATIC_PATH + 'switchTimer.txt', 'w')
			file.write(data)
			file.flush()
			file.close()
			
			switchOnTimer.clear()
			switchOffTimer.clear()
			activeTimer.clear()
			
			file = open(STATIC_PATH + 'switchTimer.txt', 'r')
		 	lines = file.readlines()
		 	file.close()
		 	for line in lines:
		 		line = line.strip('\n')
		 		val = line.split('$')
		 		switchName = val[0]
		 		onTime = val[1].split(';')
				offTime = val[2].split(';')
				switchOnTimer[switchName] = onTime		
				switchOffTimer[switchName] = offTime
				activeTimer[switchName] = val[3]
				print switchName, onTime, offTime, val[3]


	def post(self):
		global codes, clients
		
		print 'POST empfangen...'
		try:
			cmd = self.get_argument("cmd")
			
			if not cmd or not len(cmd):
			    return self.write({"success":False})
			print "command:", cmd
			
			if cmd == 'getSwitches':
				result = []
				for code in codes:
					result.append(code[0])	
				self.write({"success":True,
							"names":result})
				
			elif cmd == 'getSensors':
				result = {}				
				for id in sensorDict:
					sensorName = sensorNameDict.get(id)
					sensorVal = sensorDict.get(id)[0]					
					result[sensorName] = "%.1f" %sensorVal
				
				self.write({"success":True,
							"sensor":result})
				
			elif cmd == 'getState':
				result = []
				switchName = self.get_argument("code")
				print switchName
				result = currentSwitchStateDict.get(switchName)
				if result != None:
					result = result[0]
					if result == 0:
						result = 1
					else:
						result = 0
				else:
					result = 1
				self.write(result[0])			
				
			elif cmd == 'switchOn':
				#code = self.get_argument("code")
				switchName = self.get_argument("code")
				print switchName
				for entry in codes:
					if switchName ==  entry[0]:
						switchOn = entry[1]
						switchOff = entry[2]
						switchProtocol = entry[3]
						bitLength = entry[4]
						mode = entry[5]
						pulseWidth = entry[6]
						
						currentSwitchStateDict[switchName] = [1,time.strftime('%H:%M')]
						
						#update clients
						myResult = 'SW_:'+switchName+':1'
						for c in clients:
							c.write_message(myResult)
							
						msg = switchOn + ':' + str(switchProtocol) + ':' + str(bitLength) + ':' + str(mode) + ':' + str(pulseWidth)
						print 'try to send:' , msg
						
						#send code to arduino	
						taskQ.put(msg)
						time.sleep(.2)
						
						#autoremotePush('switch', switchName, 1)
						start_new_thread(autoremotePush, (ar, 'switch', switchName, 1,))
					
			elif cmd == 'switchOff':
				switchName = self.get_argument("code")
				print switchName
				for entry in codes:
					if switchName ==  entry[0]:
						switchOn = entry[1]
						switchOff = entry[2]
						switchProtocol = entry[3]
						bitLength = entry[4]
						mode = entry[5]
						pulseWidth = entry[6]
						
						currentSwitchStateDict[switchName] = [0,time.strftime('%H:%M')]
						
						#update clients
						myResult = 'SW_:'+switchName+':0'
						for c in clients:
							c.write_message(myResult)
						
						msg = switchOff + ':' + str(switchProtocol) + ':' + str(bitLength) + ':' + str(mode) + ':' + str(pulseWidth)
						print 'try to send:' , msg
						
						#send code to arduino	
						taskQ.put(msg)							
						time.sleep(.2)
						
						#autoremotePush('switch', switchName, 0)	
						start_new_thread(autoremotePush, (ar, 'switch', switchName, 0,))					
					
		except:
		    self.write({"success":False})
		
def saveSwitches():
	file = open(STATIC_PATH + 'switches.txt', 'w')
	for code in codes:
		out = ''
		out = code[0] + ';' 
		out += code[1] + ';' 
		out += code[2] + ';' 
		out += str(code[3]) + ';'
		out += str(code[4]) + ';'
		out += str(code[5]) + ';'
		out += str(code[6]) + ';'
		out += str(code[7]) + ';'
		out += str(code[8]) + '\n'
		
		file.write(out)
		file.flush()
	file.close()		

def addToSwitchHTMLFile(blockId,name,codeOn,codeOff,protocol,pulseWidth,imgSrc,visible):
	#print 'Add to switch data switchHTML file...'
	global template
	
	if visible == 1:
		out = ''
		
		block = ''
		if blockId == 0:
			block = 'ui-block-a'
		elif blockId == 1:
			block = 'ui-block-b'
		elif blockId == 2:
			block = 'ui-block-c'
		elif blockId == 3:
			block = 'ui-block-d'
		elif blockId == 4:
			block = 'ui-block-e'	
			
		for line in template:
			#str.replace(old, new[, max])
			if '$blockId' in line:
				line = line.replace('$blockId', block)
			if '$switchName' in line:
				line = line.replace('$switchName', name)
			if '$switchOn' in line:
				line = line.replace('$switchOn', codeOn)
			if '$switchOff' in line:
				line = line.replace('$switchOff', codeOff)
			if '$switchProtocol' in line:
				line = line.replace('$switchProtocol', str(protocol))
			if '$switchPulseWidth' in line:
				line = line.replace('$switchPulseWidth', str(pulseWidth))
			if '$switchImgSrc' in line:
				line = line.replace('$switchImgSrc', str(imgSrc))
			out += line
		
		sfile = open(STATIC_PATH + 'switchesHTML.txt', 'a')
		sfile.write(out)
		sfile.flush()
		sfile.close()		

def addToSensorHTMLFile(sensorName,sensorCode,sensorType):
	global templateSensorT, templateSensorW, templateSensorH, sensorAddedCnt, sensorNameDict
	out = ''
	id = str(sensorType) + '@' + str(sensorCode)
	
	
	print 'addToSensorHTMLFile --- sensorAddedCnt:', sensorAddedCnt, 'len(sensorNameDict):', len(sensorNameDict)
	# add a new grid row tag
	if sensorAddedCnt % 6 == 0:
		out += '<div class="row">\n'
		
	if sensorType == 'DS18B20' or sensorType == 'LM35' or sensorType == 'DHT22_Temp':
		for line in templateSensorT:
			if '$sensorName' in line:
				line = line.replace('$sensorName', sensorName)
			if '$sensorId' in line:
				line = line.replace('$sensorId', 'sensor'+str(id))
			out += line
			
	elif sensorType == 'HX711':
		for line in templateSensorW:
			if '$sensorName' in line:
				line = line.replace('$sensorName', sensorName)
			if '$sensorId' in line:
				line = line.replace('$sensorId', 'sensor'+str(id))
			out += line
			
	elif sensorType == 'Energy':
		for line in templateSensorT:
			if '$sensorName' in line:
				line = line.replace('$sensorName', sensorName)
			if '$sensorId' in line:
				line = line.replace('$sensorId', 'sensor'+str(id))
			out += line
			
	elif sensorType == 'BMP180':
		for line in templateSensorT:
			if '$sensorName' in line:
				line = line.replace('$sensorName', sensorName)
			if '$sensorId' in line:
				line = line.replace('$sensorId', 'sensor'+str(id))
			out += line
	
	elif sensorType == 'M':
		pass
	
	elif sensorType == 'DHT22_Humi':
		for line in templateSensorH:
			if '$sensorName' in line:
				line = line.replace('$sensorName', sensorName)
			if '$sensorId' in line:
				line = line.replace('$sensorId', 'sensor'+str(id))
			out += line
	
	# close the grid row tag
	sensorAddedCnt += 1
	if sensorAddedCnt % 6 == 0 or sensorAddedCnt == len(sensorNameDict):
		out += '</div>\n'
		print '# close the grid row tag  --- </div>\n'
		
	#sfile = open(STATIC_PATH + 'sensorsHTML.txt', 'a')
	sfile = open(STATIC_PATH + 'dashboardPanelsHTML.txt', 'a')
	sfile.write(out)
	sfile.flush()
	sfile.close()	
	
def addToSwitchTimerFile(value):
	file = open(STATIC_PATH + 'switchTimer.txt', 'a')
	
	out = value + '\n'
	
	file.write(out)
	file.flush()
	file.close()
	
def createSensorSVG(sCode, sName, tNodes, num):
	myStyle = Style(
	background='transparent',
	plot_background='transparent',
	foreground='rgba(0, 0, 0, 0.9)',
	foreground_light='rgba(0, 0, 0, 0.9)',
	foreground_dark='rgba(0, 0, 0, 0.2)',
	opacity='.5',
	opacity_hover='.9',
	transition='300ms ease-in',
	colors = (custom_style2.colors[num % 12], '#000000'))
	
	sensorType = sCode.split('@')[0]
	
	# create svg graphic only for temperature sensors
	if sensorType in ['DS18B20', 'LM35', 'HX711', 'DHT22_Temp', 'DHT22_Humi', 'Energy']:	
		myMin = int(tNodes.getMinimum(sCode))
		myMax = int(math.ceil(tNodes.getMaximum(sCode)))
		
		chart = pygal.Line(range=[myMin-2,myMax+2], show_minor_y_labels=False, label_font_size=20, margin=4, show_y_labels=True, show_dots=False, show_legend=False, fill=True, style=myStyle, width=500, height=100, include_x_axis=False)
		
		chart.y_labels = [(myMin-1), (((myMax-myMin)/2.0)+myMin) , (myMax+1)]
		chart.add('', tNodes.dump(sCode))
		chart.render_to_file(STATIC_PATH + 'img/' + sName + '.svg')

def createAllSensorMaxSVG(namesDict, tNodesMax):
	# create svg graphic only for temperature sensors
	myMin = []
	myMax = []
	for s in tNodesMax.getSensorList():
		sensorType = s.split('@')[0]
		if sensorType in ['DS18B20', 'LM35', 'DHT22_Temp']:
			tMin = int(tNodesMax.getMinimum(s))
			myMin.append(tMin)
			tMax = int(math.ceil(tNodesMax.getMaximum(s)))
			myMax.append(tMax)
	if len(myMin) > 0:
		myMin = min(myMin)
	else:
		myMin = 0
	if len(myMax) > 0:
		myMax = max(myMax)
	else:
		myMax = 0
		
	chart = pygal.Line(range=[myMin-2,myMax+2], show_minor_y_labels=False, label_font_size=14, margin=4, show_y_labels=True, show_dots=False, fill=False, style=custom_style2, human_readable=True, width=1000, height=200, include_x_axis=False)
	chart.y_labels = [(myMin-1), (((myMax-myMin)/2.0)+myMin) , (myMax+1)]
	
	for sensorId in namesDict:
		sensorType = sensorId.split('@')[0]
		if sensorType in ['DS18B20', 'LM35', 'DHT22_Temp']:
			sensorName = namesDict[sensorId]		
			chart.add(sensorName, tNodesMax.dump(sensorId))
	chart.render_to_file(STATIC_PATH + 'img/allMaxTempSensors.svg')
	
def createAllSensorSVG(namesDict, tNodes):
	# create svg graphic only for temperature sensors	
	myMin = []
	myMax = []
	for s in tNodes.getSensorList():
		sensorType = s.split('@')[0]
		if sensorType in ['DS18B20', 'LM35', 'DHT22_Temp']:
			tMin = int(tNodes.getMinimum(s))
			myMin.append(tMin)
			tMax = int(math.ceil(tNodes.getMaximum(s)))
			myMax.append(tMax)
	if len(myMin) > 0:
		myMin = min(myMin)
	else:
		myMin = 0
	if len(myMax) > 0:
		myMax = max(myMax)
	else:
		myMax = 0
	
	chart = pygal.Line(fill=False, interpolate='hermite', truncate_label='1', print_values=False, label_font_size=14, show_dots=True, range=[myMin-1,myMax+1], style=custom_style2, human_readable=True, show_x_guides=True, width=1000, height=200, margin=4)
	chart.x_labels = map(lambda v:"%dh" %(abs(v)), range(-24,1))
	offset = (myMax - myMin)/4.0
	chart.y_labels = [(myMin-.5), (myMin+offset), (((myMax-myMin)/2.0)+myMin), (myMax-offset), (myMax+.5)]
	
	for sensorId in namesDict:
		sensorType = sensorId.split('@')[0]
		if sensorType in ['DS18B20', 'LM35', 'DHT22_Temp']:
			sensorName = namesDict[sensorId]		
			chart.add(sensorName, tNodes.dumpAvg(sensorId))
	chart.render_to_file(STATIC_PATH + 'img/allTempSensors.svg')
	
	
class WebSocketHandler(tornado.websocket.WebSocketHandler):
	def open(self):
		print 'new connection'
		clients.append(self)
		
		self.write_message('S:connected')

	def on_message(self, message):
		global holidayMode, taskQ
		
		print 'tornado received from client: %s' % message

		#save current switch state
		val = message.split(':')
		if len(val) > 0:
			msgType = val[0].strip('\n')
			if msgType == 'SWITCH':
				if len(val) == 3:
					name = val[1]
					state = int(val[2])
	
					switchTime = time.strftime('%H:%M')
					currentSwitchStateDict[name] = [state,switchTime]
					print 'got socket message:', name, state
					
					#send current switch states to client 
					msg = 'SW_:' + name + ':' + str(state) + ':' + switchTime
					for c in clients:
						if c != self:
							c.write_message(msg)
									
					for code in codes: #if the code is known send the right code to arduino
						switchName = code[0]
						switchOn = code[1]
						switchOff = code[2]
						switchProtocol = code[3]
						bitLength = code[4]
						mode = code[5]
						pulseWidth = code[6]
						if switchName == name:
							if state == 0: #send switchOff to arduino
								msg = switchOff + ':' + str(switchProtocol) + ':' + str(bitLength) + ':' + str(mode) + ':' + str(pulseWidth)
								
							elif state == 1: #send switchOn to arduino
								msg = switchOn + ':' + str(switchProtocol) + ':' + str(bitLength) + ':' + str(mode) + ':' + str(pulseWidth)
							
							#send code to arduino	
							q = self.application.settings.get('queue')
							print 'Send to Arduino:', msg
							q.put(msg)
							time.sleep(.2)
							
							#autoremotePush('switch', name, state)
							start_new_thread(autoremotePush, (ar, 'switch', name, state,))
							
				#else:
				#	#send current switch states to client
				#	for switchName in currentSwitchStateDict:
				#		state = currentSwitchStateDict[switchName]
				#		msg = 'SW_:' + switchName + ':' + str(state)
				#		self.write_message(msg)
			
			elif msgType == 'CMD':
				if len(val) > 1:
					command = val[1].strip('\n')
					if command == 'allOff':
						#send for every code in codes the codeOff to arduino
						for code in codes:
							switchOff = code[2]
							switchProtocol = code[3]
							bitLength = code[4]
							mode = code[5]
							pulseWidth = code[6]
							msg = switchOff + ':' + str(switchProtocol) + ':' + str(bitLength) + ':' + str(mode) + ':' + str(pulseWidth)
							switchTime = time.strftime('%H:%M')
							
							#send code to arduino	
							q = self.application.settings.get('queue')
							print 'Send to Arduino:', msg
							q.put(msg)
							time.sleep(.2)
							
							#send off states to clients 
							msg = 'SW_:' + code[0] + ':0:' + switchTime 
							for c in clients:
								c.write_message(msg)
								
							#save the new state to dictionary
							currentSwitchStateDict[code[0]] = [0,switchTime]
							
					elif command == 'delete':
						#delete the given switch from codes
						if len(val) > 2:
							switchName = val[2].strip('\n')
							found = True
							while found:
								for code in codes:
									if switchName == code[0]:
										found = True
										codes.remove(code)
										print 'Removed switch:', switchName
										break
									else:
										found = False
										
							# Delete HTML File for switches and build it up new
							print "Delete an existing HTML file for switches..."
							file = open(STATIC_PATH + 'switchesHTML.txt', 'w')
							file.close()
							
							# Write all other switches back to file
							print "Save switches information..."						
							saveSwitches()
							
							# Delete switch from dictionary
							currentSwitchStateDict.pop(switchName, None)
							switchStopWatchDict.pop(switchName, None)
			
							print "Generating new switchHTML with sorted switches..."
							for i,code in enumerate(codes):
								blockId = i%5
								print 'BlockId:',blockId,'Switch:',code[0],'On:',code[1],'Off:',code[2],'Protocol:',str(code[3]),'Bits:',str(code[4]),'Mode:',str(code[5]),'PulseWidth:',str(code[6])
								addToSwitchHTMLFile(blockId,code[0],code[1],code[2],code[3],code[6],code[7],code[8])
															
					elif command == 'update':
						#update on/off times for a switch
						if len(val) > 3:
							switchName = val[2].strip('\n')
							
							#combine the last strings in val list to one string
							timeStr = ''
							for i in range(3,len(val)):
								timeStr += val[i] + ':'
								
							timeStr = timeStr[:-1]
							timeVal = timeStr.split('$')
							if len(timeVal) == 3:
								onTime = timeVal[0].split(';')
								offTime = timeVal[1].split(';')
								active = timeVal[2].strip('\n')
								print 'onTime:', onTime
								print 'offTime:', offTime
								print 'active:', active								
								
								writeFile = False
								
								if len(onTime) == 0 and len(offTime) == 0:
									if switchName in activeTimer.keys():
										del activeTimer[switchName]
								else:
									activeTimer[switchName] = active
								
								if len(onTime) > 0:
									switchOnTimer[switchName] = onTime									
									writeFile = True
								elif switchName in switchOnTimer.keys():
									del switchOnTimer[switchName]
									
								if len(offTime) > 0:
									switchOffTimer[switchName] = offTime
									writeFile = True	
								elif switchName in switchOffTimer.keys():
									del switchOffTimer[switchName]
									
								# read in the switchTimer file
								file = open(STATIC_PATH + 'switchTimer.txt', 'r')
								lines = file.readlines()
								file.close()
								
								# rewrite the switchTimer file without the current switch times
								file = open(STATIC_PATH + 'switchTimer.txt', 'w')
								out = ''
								for line in lines:
									name = line.split('$')[0]
									if name != switchName:
										out += line
								file.write(out)
								file.flush()
								file.close()	
									
								# add new time values to switchTimer file
								if writeFile:
									addToSwitchTimerFile(switchName + '$' + timeStr)
									
					elif command == 'getStatus':
						tempRead = os.popen("vcgencmd measure_temp")
						lines = tempRead.readlines()
						tempRead.close()
						
						l=lines[0].split('=')
						temp = l[1].strip('\n')
						
						dateRead = os.popen("uptime")
						lines = dateRead.readlines()
						dateRead.close()
						uptime = lines[0].split(',')[0]						
						
						print temp, uptime
						
						#send off states to clients 
						msg = 'STATUS:' + temp + ':' + uptime
						for c in clients:
							c.write_message(msg)
							
					elif command == 'dumpStopWatch':
						for name in switchStopWatchDict:
							stopWatch = switchStopWatchDict[name]
							#print 'Switch: %s - On: %s - Power usage: %s' %(name, stopWatch.getTime(), stopWatch.getPowerUsageKWh())
							
					elif command == 'shutdown':
						os.popen("sudo shutdown now")
						
					elif command == 'reboot':
						os.popen("sudo reboot")
						
					elif command == 'updateHolidayMode':
						if len(val) > 2:
							mode = int(val[2].strip('\n'))
							if mode == 1:
								print 'set holidayMode: 1'
								holidayMode = True
							else:
								mode = 0
								print 'set holidayMode: 0'
								holidayMode = False
								
							file = open(STATIC_PATH + 'switchHolidayMode.txt','w')
							file.write(str(mode)+'\n')
							file.flush()
							file.close()
							
					elif command == 'RGBStripe':
						if len(val) > 2:
							code = int(val[2].strip('\n'))
							msg = str(code) + ':1:32:1:345'
							taskQ.put(msg)							
							time.sleep(.2)
							
					elif command == 'UpdateThumbs':
						fileDir = STATIC_PATH + 'img/thumbs/'
						input_files = os.listdir(fileDir)
						
						thumbFile = open(STATIC_PATH + 'thumbnails.txt', 'w')
							
						if len(input_files) > 0:						
							for i, input_filename in enumerate(input_files):
								
								thumbnail = input_filename.split('.')[0]								
								#write to thumbnails file
								m = i % 5								
								if m == 0:
									myStr = '<div class="ui-block-a"><div class="myThumb" style="background-image: url(static/img/thumbs/'+ thumbnail +'.jpg)" id="'+ thumbnail +'"></div></div>\n'
									thumbFile.write(myStr)
									thumbFile.flush()
									
								elif m == 1:
									myStr = '<div class="ui-block-b"><div class="myThumb" style="background-image: url(static/img/thumbs/'+ thumbnail +'.jpg)" id="'+ thumbnail +'"></div></div>\n'
									thumbFile.write(myStr)
									thumbFile.flush()
									
								elif m == 2:
									myStr = '<div class="ui-block-c"><div class="myThumb" style="background-image: url(static/img/thumbs/'+ thumbnail +'.jpg)" id="'+ thumbnail +'"></div></div>\n'
									thumbFile.write(myStr)
									thumbFile.flush()
									
								elif m == 3:
									myStr = '<div class="ui-block-d"><div class="myThumb" style="background-image: url(static/img/thumbs/'+ thumbnail +'.jpg)" id="'+ thumbnail +'"></div></div>\n'
									thumbFile.write(myStr)
									thumbFile.flush()
								
								elif m == 4:
									myStr = '<div class="ui-block-e"><div class="myThumb" style="background-image: url(static/img/thumbs/'+ thumbnail +'.jpg)" id="'+ thumbnail +'"></div></div>\n'
									thumbFile.write(myStr)
									thumbFile.flush()
																	
						thumbFile.close()
											
										
			elif msgType == 'updatedata':
				print 'update all switches to client'
				
				#send current switch states to client
				for switchName in currentSwitchStateDict:
					state = currentSwitchStateDict[switchName][0]
					switchTime = currentSwitchStateDict[switchName][1]
					msg = 'SW_:' + switchName + ':' + str(state) + ':' + switchTime
					self.write_message(msg)
					
			elif msgType == 'updatesensors':
				print 'update all sensors to all clients'
				for sensorId in sensorDict:
					sensorVal = sensorDict[sensorId][0]
					sensorBattery = sensorDict[sensorId][1]
					sensorTime = sensorDict[sensorId][2]
					sensorMin = tempSensorNodes.getMinimum(sensorId)
					sensorMax = tempSensorNodes.getMaximum(sensorId)
					if len(sensorDict[sensorId]) == 3:
						msg = 'SENSOR:'+str(sensorId)+':'+str(sensorVal)+':'+str(sensorBattery)+':'+str(sensorTime)+':'+str(sensorMin)+':'+str(sensorMax)
					elif len(sensorDict[sensorId]) == 4:
						#workaround to update Energy values
						timeStr = time.strftime('%H:%M:%S', time.localtime(sensorTime))	
						msg = 'SENSOR:'+str(sensorId)+':'+str(round(sensorVal,2))+':'+str(sensorBattery)+':'+ timeStr +':'+str(round(sensorMin,2))+':'+str(round(sensorMax,2))
					self.write_message(msg)
								
	def on_close(self):
		print 'connection closed'
		clients.remove(self)

################################ MAIN ################################
def main():
	global taskQ, template, codes, holidayMode, switchStopWatchDict, currentSwitchStateDict, sensorDict, tempSensorNodes, tempSensorNodesMax
	global templateSensorT, templateSensorL, templateSensorM, templateSensorH, sensorNameDict, p, templateSensorW, ar
	
	out = time.strftime('%a, %d %b %Y, %H:%M')
	print '##### Smart Home started at %s #####' %out
	
	if os.path.exists(STATIC_PATH + 'switchTemplate.txt') == False:
		print 'Could not find the switch html template!'
		quit()
		
	if os.path.exists(STATIC_PATH + 'sensorTempTemplate2.txt') == False:
		print 'Could not find the temperature sensor html template!'
		quit()
		
	if os.path.exists(STATIC_PATH + 'sensorWeightTemplate.txt') == False:
		print 'Could not find the weight sensor html template!'
		quit()
		
	if os.path.exists(STATIC_PATH + 'sensorHumiTemplate.txt') == False:
		print 'Could not find the humidity sensor html template!'
		quit()
	
	if os.path.exists(STATIC_PATH + 'switches.txt') == False:
		file = open(STATIC_PATH + 'switches.txt', 'w')
		file.close()
		
	if os.path.exists(STATIC_PATH + 'switchTimer.txt') == False:
		file = open(STATIC_PATH + 'switchTimer.txt', 'w')
		file.close()
		
	if os.path.exists(STATIC_PATH + 'switchHolidayMode.txt') == False:
		file = open(STATIC_PATH + 'switchHolidayMode.txt', 'w')
		file.write('0\n')
		file.flush()
		file.close()
		
	if os.path.exists(STATIC_PATH + 'switchStopWatch.txt') == False:
		file = open(STATIC_PATH + 'switchStopWatch.txt', 'w')
		file.close()
		
	if os.path.exists(STATIC_PATH + 'sensors.txt') == False:
		file = open(STATIC_PATH + 'sensors.txt', 'w')
		file.close()
	
	if os.path.exists(STATIC_PATH + 'sensorMinMax.txt') == False:
		file = open(STATIC_PATH + 'sensorMinMax.txt', 'w')
		file.close()
	else:
		# read in old values, check the date to create correct graph
		file = open(STATIC_PATH + 'sensorMinMax.txt', 'r')
		lines = file.readlines()
		file.close()
		
		for line in lines:
			val = line.split(';')
			t = val[0]
			for i in range(1,len(val)):
				sensorId = val[i].split('$')[0]
				sensorMin = float(val[i].split('$')[1])
				sensorMax = float(val[i].split('$')[2])
				
				tempSensorNodesMax.shiftValue(sensorId)
				tempSensorNodesMax.replaceValue(sensorId, sensorMax)
	
	# Load the HTML Template for switch setup
	print "Load switch template..."
	file = open(STATIC_PATH + 'switchTemplate.txt', 'r')
	template = file.readlines()
	file.close()

	# Load the HTML Template for temperature sensor
	print "Load sensor template..."
	file = open(STATIC_PATH + 'sensorTempTemplate2.txt', 'r')
	templateSensorT = file.readlines()
	file.close()
	
	# Load the HTML Template for weight sensor
	print "Load sensor template..."
	file = open(STATIC_PATH + 'sensorWeightTemplate.txt', 'r')
	templateSensorW = file.readlines()
	file.close()
	
	# Load the HTML Template for humidity sensor
	print "Load sensor template..."
	file = open(STATIC_PATH + 'sensorHumiTemplate.txt', 'r')
	templateSensorH = file.readlines()
	file.close()
	
	# Delete HTML File for sensors and build it up new
	print "Delete an existing HTML file for sensors..."
	#file = open(STATIC_PATH + 'sensorsHTML.txt', 'w')
	file = open(STATIC_PATH + 'dashboardPanelsHTML.txt', 'w')
	file.close()
	
	# Build new HTML file for the switches
	print "Build new sensor HTML file..."
	file = open(STATIC_PATH + 'sensors.txt', 'r')
 	lines = file.readlines()
 	file.close()
 	for line in lines:
		val = line.split(';')
		sensorName = val[0]
		sensorCode = int(val[1])
		sensorType = val[2].strip()
		
		id = str(sensorType) + '@' + str(sensorCode)
		if id not in sensorNameDict.keys():
			sensorNameDict[id] = sensorName
			
	for id in sensorNameDict:
		sensorName = sensorNameDict[id]
		sensorType = id.split('@')[0]
		sensorCode = id.split('@')[1]		
		addToSensorHTMLFile(sensorName,sensorCode,sensorType)
	
	# create empty sensor svg files
	if os.path.exists(STATIC_PATH + 'img/allTempSensors.svg') == False:
		for sensorId in sensorNameDict:
			tempSensorNodes.shiftValue(sensorId)
		#createSensorSVG()
		#start_new_thread(createAllSensorSVG, (sensorNameDict, tempSensorNodes,))	
	
	# Delete HTML File for switches and build it up new
	print "Delete an existing HTML file for switches..."
	file = open(STATIC_PATH + 'switchesHTML.txt', 'w')
	file.close()
	
	# Build new HTML file for the switches
	print "Build new switch HTML file..."
 	file = open(STATIC_PATH + 'switches.txt', 'r')
 	lines = file.readlines()
 	file.close()
	for line in lines:
		val = line.split(';')
		if len(val) != 9:
			print 'ERROR: Wrong switch definition!'
			print 'Syntax: "Switch name;CodeOn;CodeOff;Protocol;Bitlength;Mode;Pulsewidth;Image source;Visible"'
			print 'Example: "Wohnzimmer IKEA 1;21811;21820;1;24;1;184;Ikea_Wohnzimmer;1"'
		else:
			switchName = val[0]
			codeOn = val[1]
			codeOff = val[2]
			protocol = int(val[3])
			bitLength = int(val[4])
			mode = int(val[5])
			pulseWidth = int(val[6])
			#imgSrc = val[7].strip('\n')
			imgSrc = val[7]
			visible = int(val[8])
			
			codes.append([switchName, codeOn, codeOff, protocol, bitLength, mode, pulseWidth, imgSrc, visible])
			currentSwitchStateDict[switchName] = [0,time.strftime('%H:%M')]
			switchStopWatchDict[switchName] = StopWatch()
	
	codes.sort()
	for i,code in enumerate(codes):
		blockId = i%5
		print 'BlockId:',blockId,'Switch:',code[0],'On:',code[1],'Off:',code[2],'Protocol:',str(code[3]),'Bits:',str(code[4]),'Mode:',str(code[5]),'PulseWidth:',str(code[6])
		addToSwitchHTMLFile(blockId,code[0],code[1],code[2],code[3],code[6],code[7],code[8])	

	file = open(STATIC_PATH + 'switchTimer.txt', 'r')
 	lines = file.readlines()
 	file.close()
 	for line in lines:
 		line = line.strip('\n')
 		val = line.split('$')
 		switchName = val[0]
 		onTime = val[1].split(';')
		offTime = val[2].split(';')
		switchOnTimer[switchName] = onTime		
		switchOffTimer[switchName] = offTime
		activeTimer[switchName] = val[3]
		print switchName, onTime, offTime, val[3]
		
	file = open(STATIC_PATH + 'switchHolidayMode.txt', 'r')
	lines = file.readlines()
 	file.close()
 	if len(lines) > 0:
 		val = int(lines[0].strip('\n'))
 		if val == 1:
 			holidayMode = True
 			print 'HolidayMode: 1'
 		else:
 			holidayMode = False
 			print 'HolidayMode: 0'
 			
 	if os.path.isfile(STATIC_PATH + 'autoremotekey'):
 		_url = [_f for _f in open(STATIC_PATH + 'autoremotekey','r').read().split("\n") if _f]
 		print _url[0]
 		ar = autoremote(url=_url[0])   # Connect to AutoRemote server
 	 	ar.register(name="Smarthome")   # Register device
 	 	
 	 	start_new_thread(autoremotePushMessage, (ar, 'Smarthome started...'))
 			
 	'''
 	if os.path.isfile(STATIC_PATH + 'myapikey'):
		keys = [_f for _f in open(STATIC_PATH + 'myapikey','r').read().split("\n") if _f]		
		p = PyNMA(keys)
		p.push("SmartHome", 'Status', 'successfully started...', batch_mode=False)
	else:
		print("need a file named myapikey containing one apikey per line")
	
	'''
	taskQ = multiprocessing.Queue()
	resultQ = multiprocessing.Queue()

	sp = serialProcess.SerialProcess(taskQ, resultQ)
	sp.daemon = True
	sp.start()
	
	# wait a second before sending first task
	time.sleep(1)
	taskQ.put("first task")
	
	tornado.options.parse_command_line()	
	
	#settings = {
	#"cookie_secret": "61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
	#"login_url": "/login",
	#"xsrf_cookies": True,
	#}

	app = tornado.web.Application(
	    handlers=[
	    	(r"/static/(.*)", MyStaticFileHandler, {'path': '/home/pi/smart-home-v2/static'}),
	        (r"/", IndexHandler),
	        (r"/ws", WebSocketHandler),
	        (r"/upload", UploadHandler),
	        (r"/api/switch/(.*)", SwitchAPIHandler)
	    ], queue=taskQ#, **settings
	)
	httpServer = tornado.httpserver.HTTPServer(app)
	httpServer.listen(options.port)
	print "Listening on port:", options.port
	
	def checkSwitchTimer():
		global lastTime
		currentTime = time.strftime('%H:%M')
		currentTimeMinutes = int(time.strftime('%M'))
		
		# heutiger Tag
		n = int(time.strftime('%u'))
		
		# bitmask value vom heutigen tag
		todayMask = 2**(n-1)
		
		if currentTime != lastTime:
			if currentTime == '00:00' or currentTime == '12:00':
				file = open(STATIC_PATH + 'switchStopWatch.txt', 'a')
				out = time.strftime('--- %a, %d %b %Y, %H:%M ---') + '\n'
				for name in switchStopWatchDict:
					stopWatch = switchStopWatchDict[name]					
					hours = stopWatch.getHoursDaily()
					stopWatch.resetDaily()
					if hours > 0.0:
						out += '%s: %.4f hours\n' %(name, hours)
				file.write(out)
				file.flush()
				file.close()
			
			# save min + max sensor values to file
			if currentTime == '00:00':
				myStr = str(time.time())
				for sensorId in tempSensorNodes.getSensorList():
					sensorMin = tempSensorNodes.getMinimum(sensorId)
					sensorMax = tempSensorNodes.getMaximum(sensorId)
					myStr += ';' + str(sensorId) + '$' + str(sensorMin) + '$' + str(sensorMax)
					
					tempSensorNodesMax.shiftValue(sensorId)
					tempSensorNodesMax.replaceValue(sensorId, sensorMax)
					
				myStr += '\n'
				file = open(STATIC_PATH + 'sensorMinMax.txt', 'a')
				file.write(myStr)
				file.flush()
				file.close()
				
				#start_new_thread(createAllSensorMaxSVG, (sensorNameDict, tempSensorNodesMax,))
				
			#send status messages to NMA 
			if currentTime in ['11:00', '13:00', '15:00', '17:00']:
				if p!=None:
					msg = ''
					for sensorId in sensorDict:
						sensorVal = sensorDict[sensorId][0]
						sensorBattery = sensorDict[sensorId][1]
						sensorTime = sensorDict[sensorId][2]
						sensorMin = tempSensorNodes.getMinimum(sensorId)
						sensorMax = tempSensorNodes.getMaximum(sensorId)
						sensorName = sensorNameDict[sensorId]
						msg += sensorName + ':' + str(sensorVal) + 'C Max:' + str(sensorMax) + 'C Min:' + str(sensorMin) + 'C Time:' + str(sensorTime) +'\n'
					
					p.push("SmartHome", 'Status', msg, batch_mode=False)  	
				
			# create a small svg for every tempsensor every minute
			'''	
			if currentTimeMinutes in range(0,60,10):
				cnt = 0
				for sensorId in sensorNameDict:
					sensorName = sensorNameDict[sensorId]
					start_new_thread(createSensorSVG, (sensorId, sensorName, tempSensorNodes,cnt,))
					cnt += 1
					
				start_new_thread(createAllSensorSVG, (sensorNameDict, tempSensorNodes,))
			'''
				
			for sensorId in sensorDict:
				tempSensorNodes.shiftValue(sensorId)
		
			# for every activated switch add a minute to the corresponding stopwatch
			for name in currentSwitchStateDict:
				if currentSwitchStateDict[name][0] == 1:
					stopWatch = switchStopWatchDict.get(name)
					if stopWatch != None:
						stopWatch.add()
				
			for name in switchOnTimer:
				active = activeTimer.get(name)
				if active == '1':
					onTime = switchOnTimer[name]
					onTimeDict = {}

					for t in onTime:
						val = t.split('-')
						if len(val) > 1:
							onTimeDict[val[0]] = int(val[1])
					
					if currentTime in onTimeDict.keys():
						mask = onTimeDict[currentTime]
						if holidayMode: # send codes for Sa and So every day
							if 96 & mask:
								#switch on and send update to clients					
								for code in codes: #if the code is known send the right code to arduino
									switchName = code[0]
									switchOn = code[1]
									switchOff = code[2]
									switchProtocol = code[3]
									bitLength = code[4]
									mode = code[5]
									pulseWidth = code[6]
									if switchName == name:
										msg = switchOn + ':' + str(switchProtocol) + ':' + str(bitLength) + ':' + str(mode) + ':' + str(pulseWidth)
										
										#send code to arduino	
										taskQ.put(msg)							
										time.sleep(.2)
										
										# send switch status change to autoremote
										#autoremotePush('switch', switchName, 1)
										start_new_thread(autoremotePush, (ar, 'switch', switchName, 1,))
										
										switchTime = time.strftime('%H:%M')		
										currentSwitchStateDict[name] = [1,switchTime]
			
										#update clients
										myResult = 'SW_:'+switchName+':1:'+switchTime
										sendToClients(myResult)
						else:
							if todayMask & mask:
								#switch on and send update to clients					
								for code in codes: #if the code is known send the right code to arduino
									switchName = code[0]
									switchOn = code[1]
									switchOff = code[2]
									switchProtocol = code[3]
									bitLength = code[4]
									mode = code[5]
									pulseWidth = code[6]
									if switchName == name:
										msg = switchOn + ':' + str(switchProtocol) + ':' + str(bitLength) + ':' + str(mode) + ':' + str(pulseWidth)
										
										#send code to arduino	
										taskQ.put(msg)							
										time.sleep(.2)
										
										# send switch status change to autoremote
										#autoremotePush('switch', switchName, 1)
										start_new_thread(autoremotePush, (ar, 'switch', switchName, 1,))
											
										switchTime = time.strftime('%H:%M')	
										currentSwitchStateDict[name] = [1,switchTime]
			
										#update clients
										myResult = 'SW_:'+switchName+':1:'+switchTime
										sendToClients(myResult)
						
			for name in switchOffTimer:
				active = activeTimer.get(name)
				if active == '1':
					offTime = switchOffTimer[name]
					offTimeDict = {}
					
					for t in offTime:
						val = t.split('-')
						if len(val) > 1:
							offTimeDict[val[0]] = int(val[1])

					if currentTime in offTimeDict.keys():
						mask = offTimeDict[currentTime]
						if holidayMode: # send codes for Sa and So every day
							if 96 & mask:
								#switch off and send update to clients
								for code in codes: #if the code is known send the right code to arduino
									switchName = code[0]
									switchOn = code[1]
									switchOff = code[2]
									switchProtocol = code[3]
									bitLength = code[4]
									mode = code[5]
									pulseWidth = code[6]
									if switchName == name:
										msg = switchOff + ':' + str(switchProtocol) + ':' + str(bitLength) + ':' + str(mode) + ':' + str(pulseWidth)
										
										#send code to arduino	
										taskQ.put(msg)							
										time.sleep(.2)
										
										# send switch status change to autoremote
										#autoremotePush('switch', switchName, 0)
										start_new_thread(autoremotePush, (ar, 'switch', switchName, 0,))
										
										switchTime = time.strftime('%H:%M')
										currentSwitchStateDict[name] = [0,switchTime]
									
										#update clients
										myResult = 'SW_:'+switchName+':0:'+switchTime
										sendToClients(myResult)
						else:
							if todayMask & mask:
								#switch off and send update to clients
								for code in codes: #if the code is known send the right code to arduino
									switchName = code[0]
									switchOn = code[1]
									switchOff = code[2]
									switchProtocol = code[3]
									bitLength = code[4]
									mode = code[5]
									pulseWidth = code[6]
									if switchName == name:
										msg = switchOff + ':' + str(switchProtocol) + ':' + str(bitLength) + ':' + str(mode) + ':' + str(pulseWidth)
										
										#send code to arduino	
										taskQ.put(msg)							
										time.sleep(.2)
										
										# send switch status change to autoremote
										#autoremotePush('switch', switchName, 0)
										start_new_thread(autoremotePush, (ar, 'switch', switchName, 0,))
										
										switchTime = time.strftime('%H:%M')
										currentSwitchStateDict[name] = [0,switchTime]
									
										#update clients
										myResult = 'SW_:'+switchName+':0:'+switchTime
										sendToClients(myResult)
							
			lastTime = currentTime
		
	def checkResults():
		global luxAvg, voltsAvg, lsCnt, lastTimeTemp, lastSensorId, sensorDict, rfs, lastCode
		
		checkSwitchTimer()
		
		if not resultQ.empty():
			result = resultQ.get()
				
			# arduino hat einen Code empfangen, wenn von einem bekannten switch den entsprechenden state an die clients senden
			val = result.split(':')
			if val[0] == 'SW':
				curTime = time.strftime('%d %m %Y, %H:%M')
				#print curTime + "tornado received from arduino: " + result
				try:
					protocol = int(val[1])
					codeDec = str(val[2])
					codeHex = str(val[3])
					bitLength = int(val[4])
					pulseWidth = int(val[5])
				except:
					print 'Something went wrong with the sent serial command from arduino!'
					protocol = 0
					codeDec = "0"
					codeHex = "0"
					bitLength = 0
					pulseWidth = 0
				
				shouldSend = 1
				
				currentTime = time.strftime('%H:%M')
				if codeDec != lastCode or lastTime != currentTime:
				
					####################################################
					# decode message and check if we have sensor values
					#print codeDec
					if rfs.decode(codeDec):
						id = str(rfs.sensorType) + '@' + str(rfs.sensorCode)
												
						#if curTime != lastTimeTemp or id != lastSensorId:						
	
						if rfs.sensorType == 'Energy':
							#t = time.strftime('%H:%M:%S')
							t = time.time()					
							
							lastSensorValue = sensorDict.get(id)
							energyValue = 0
							
							if lastSensorValue != None:
								print lastSensorValue
								energyOld = lastSensorValue[1]
								energyCur = rfs.sensorValue # given in Wh
	
								#Momentanverbrauch = ( gesamtNeu - gesamtAlt ) * ( 3600 / ( zeitNeu - zeitAlt ) )
								energyDiff = energyCur - energyOld
								tOld = lastSensorValue[2] # in seconds
								
								timeDiff = t - tOld
								
								
								# Momentanverbrauch
								if (timeDiff > 1.0): # time difference greater than 1 second
									print "time difference is", timeDiff, 'seconds'
									energyValue = energyDiff * (3600.0 / float(timeDiff))	
									#print 'energyValue:', energyValue
								else:
									#print '<=0'
									#print "time difference is 0 seconds"
									energyValue = lastSensorValue[0]
									#print 'energyValue:', energyValue		
								
							tempSensorNodes.replaceValue(id, energyValue)
							
							# Momentanverbrauch, Gesamtverbrauch, Zeitstempel
							sensorDict[id] = [energyValue, rfs.sensorValue, t, True]
							sensorMin = tempSensorNodes.getMinimum(id)
							sensorMax = tempSensorNodes.getMaximum(id)
							
							timeStr = time.strftime('%H:%M:%S', time.localtime(t))	
							
							myResult = 'SENSOR:'+str(id)+':'+str(round(energyValue,2))+':'+str(rfs.sensorValue)+':'+ timeStr +':'+str(round(sensorMin,2))+':'+str(round(sensorMax,2))
							print 'Energy SENSOR: ' + str(rfs.sensorValue) + 'Wh  ' + str(round(energyValue,2)) + 'W'
							
							
							#send value to influxDB
							saveSensorValue(id, energyValue)
							
						else:
							t = time.strftime('%H:%M') 						
							tempSensorNodes.replaceValue(id, rfs.sensorValue)
							
							sensorDict[id] = [rfs.sensorValue, rfs.sensorBattery, t] 						
							sensorMin = tempSensorNodes.getMinimum(id)
							sensorMax = tempSensorNodes.getMaximum(id)
							myResult = 'SENSOR:'+str(id)+':'+str(rfs.sensorValue)+':'+str(rfs.sensorBattery)+':'+str(t)+':'+str(sensorMin)+':'+str(sensorMax)
							
							
							print myResult
							
							#send value to influxDB
							saveSensorValue(id, rfs.sensorValue)
							
							#send value to autoremote
							#autoremotePush('sensor', id, rfs.sensorValue)
							sensorName = sensorNameDict.get(id)
							start_new_thread(autoremotePush, (ar, 'sensor', sensorName, rfs.sensorValue,))
							
							
						sendToClients(myResult)						
						#end if						
	 						
	 					lastTimeTemp = curTime
	 					lastSensorId = id
	
					for entry in codes:
						#print 'check entry:', entry
						name = entry[0]
						if (codeDec == entry[1] or codeHex == entry[1]) and protocol == int(entry[3]): # Schalter ein
							switchTime = time.strftime('%H:%M')
							currentSwitchStateDict[name] = [1,switchTime]
							shouldSend = 0
							
							myResult = 'SW_:'+name+':1:'+switchTime
							sendToClients(myResult)
							#autoremotePush('switch', name, 1)
							start_new_thread(autoremotePush, (ar, 'switch', name, 1,))
							print myResult
							
						elif (codeDec == entry[2] or codeHex == entry[2]) and protocol == int(entry[3]): # Schalter aus
							switchTime = time.strftime('%H:%M')
							currentSwitchStateDict[name] = [0,switchTime]
							shouldSend = 0
							
							myResult = 'SW_:'+name+':0:'+switchTime
							sendToClients(myResult)
							#autoremotePush('switch', name, 0)
							start_new_thread(autoremotePush, (ar, 'switch', name, 0,))
							print myResult
							
					if shouldSend:
						#print 'unkown code'
						result = 'SW: Protocol: ' + str(protocol) + ' Dec: ' + codeDec + ' Hex: ' + codeHex + ' Bits: ' + str(bitLength) + ' Pulse width: ' + str(pulseWidth)
						sendToClients(result)
						
				lastCode = codeDec					

	def addToLdrHistory(lux,volts):
		file = open(STATIC_PATH + 'ldrHistory.txt', 'a')
		date = time.strftime('%Y-%m-%d %H:%M')
		out = str(date) + ';' + str(lux) + ';' + str(volts) +'\n'
		file.write(out)
		file.flush()
		file.close()		
				
	def sendToClients(msg):
		for c in clients:
			#print 'send result to clients:', msg
			c.write_message(msg)

	mainLoop = tornado.ioloop.IOLoop.instance()
	scheduler = tornado.ioloop.PeriodicCallback(checkResults, 10, io_loop = mainLoop)
	scheduler.start()
	mainLoop.start()
 
if __name__ == "__main__":
	main()