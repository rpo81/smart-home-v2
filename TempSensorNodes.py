import time

class TempSensorNodes():
	def __init__(self):
		self.sensorDict = {}
	
	def addValue(self, sensorCode, time, sensorTemp):
		if self.sensorDict.get(sensorCode) != None:
			self.sensorDict[sensorCode].append([time, sensorTemp])
		else:
			self.sensorDict[sensorCode] = [[time, sensorTemp]]
			
	def getLastHour(self, sensorCode, specificTime=None):		
		# get current time
		if specificTime == None:
			currentMinute = int(time.strftime('%M'))
			currentHour = int(time.strftime('%H'))
			currentDay = int(time.strftime('%d'))
			currentMonth = int(time.strftime('%m'))
			currentYear = int(time.strftime('%Y'))
		else:
			currentMinute = specificTime[0]
			currentHour = specificTime[1]
			currentDay = specificTime[2]
			currentMonth = specificTime[3]
			currentYear = specificTime[4]
		
		# from which time we should begin
		startHour = (currentHour - 1) % 24
		startMinute = currentMinute
		if startHour == 23:
			if currentDay > 1:
				startDay = currentDay - 1
				startMonth = currentMonth
				startYear = currentYear 
			else:
				startMonth = (currentMonth - 1) % 12
				if currentMonth == 1:
					startYear = currentYear - 1
					startDay = 31
				elif currentMonth == 2:
					startDay = 31
				elif currentMonth == 3:
					startDay = 28
				elif currentMonth == 4:
					startDay = 31
				elif currentMonth == 5:
					startDay = 30
				elif currentMonth == 6:
					startDay = 31
				elif currentMonth == 7:
					startDay = 30
				elif currentMonth == 8:
					startDay = 31
				elif currentMonth == 9:
					startDay = 31
				elif currentMonth == 10:
					startDay = 30
				elif currentMonth == 11:
					startDay = 31
				elif currentMonth == 12:
					startDay = 30				
		else:
			startDay = currentDay
			startMonth = currentMonth
			startYear = currentYear
			
		minute = startMinute
		hour = startHour
		day = startDay
		month = startMonth
		year = startYear
		
		result = []
		
		for i in range(60):
			# check if time in dictionary and append result list
			temp = self.checkTime(sensorCode, minute, hour, day, month, year)
			result.append(temp)
			
			minute = (minute + 1) % 60
			if minute == 0 :
				hour = currentHour
				day = currentDay
				month = currentMonth
				year = currentYear
				
		return result
		
	def checkTime(self, sensorCode, minute, hour, day, month, year):
		val = self.sensorDict.get(sensorCode)
		if val != None:
			str = '%02d %02d %d, %02d:%02d' %(day, month, year, hour, minute)
			for v in val:
				if v[0] == str:
					return float(v[1])
		return None

	def getLast24Hours(self, sensorCode):
		currentHour = int(time.strftime('%H'))
		currentDay = int(time.strftime('%d'))
		currentMonth = int(time.strftime('%m'))
		currentYear = int(time.strftime('%Y'))
		
		startHour = currentHour
		startMonth = currentMonth
		startYear = currentYear
		if currentDay == 1:
			startMonth = (currentMonth - 1) % 12
			if startMonth == 12:
				startYear = currentYear - 1
				
			if currentMonth == 1:
				startDay = 31
			elif currentMonth == 2:
				startDay = 31
			elif currentMonth == 3:
				startDay = 28
			elif currentMonth == 4:
				startDay = 31
			elif currentMonth == 5:
				startDay = 30
			elif currentMonth == 6:
				startDay = 31
			elif currentMonth == 7:
				startDay = 30
			elif currentMonth == 8:
				startDay = 31
			elif currentMonth == 9:
				startDay = 31
			elif currentMonth == 10:
				startDay = 30
			elif currentMonth == 11:
				startDay = 31
			elif currentMonth == 12:
				startDay = 30
		else:
			startDay = currentDay - 1
			
		hour = startHour
		day = startDay
		month = startMonth
		year = startYear
		
		result = []
		hourList = []
		
		for i in range(24):
			#print 'getLastHour(sensorCode, [0,', hour, day, month, year
			val = self.getLastHour(sensorCode, [0, hour, day, month, year])
			avg = 0
			number = 0
			for v in val:
				if v != None:
					number += 1
					avg += v
			if number > 0:
				avg /= number
				result.append(avg)
				#print avg
			else:
				result.append(None)	
				#print 'None'
			
			hour = (hour + 1) % 24
			if hour == 0:
				day = currentDay
				month = currentMonth
				year = currentYear
				
			hourList.append(hour)
				
		return result, hourList	
	
	def reset(self):
		self.sensorDict.clear()
		
	def isOlder(self, toTest, day, month, year):
		val = toTest.split(',')
		testDay = int(val[0].split()[0])
		testMonth = int(val[0].split()[1])
		testYear = int(val[0].split()[2])
		
		if testDay < day or testMonth < month or testYear < year:
			return True
		return False
	
	def kickoffOldValues(self):
		currentDay = int(time.strftime('%d'))
		currentMonth = int(time.strftime('%m'))
		currentYear = int(time.strftime('%Y'))
		
		startDay = currentDay - 2
		startMonth = currentMonth
		startYear = currentYear
		
		if currentDay <= 2:
			startMonth = (currentMonth - 1) % 12
			if startMonth == 12:
				startYear = currentYear - 1
			
			if currentMonth == 1:
				startDay %= 31
			elif currentMonth == 2:
				startDay %= 31
			elif currentMonth == 3:
				startDay %= 28
			elif currentMonth == 4:
				startDay %= 31
			elif currentMonth == 5:
				startDay %= 30
			elif currentMonth == 6:
				startDay %= 31
			elif currentMonth == 7:
				startDay %= 30
			elif currentMonth == 8:
				startDay %= 31
			elif currentMonth == 9:
				startDay %= 31
			elif currentMonth == 10:
				startDay %= 30
			elif currentMonth == 11:
				startDay %= 31
			elif currentMonth == 12:
				startDay %= 30

		for sensorCode in self.sensorDict:
			val = self.sensorDict[sensorCode]
			if val != None:
				while len(val) > 0 and self.isOlder(val[0][0], startDay, startMonth, startYear):
					val.pop(0)
			
	def dump(self, sensorCode):
		val = self.sensorDict.get(sensorCode)
		if val != None:
			print 'Dump:', sensorCode, len(val) 
			if len(val) > 0:
				print 'First entry:', val[0]
